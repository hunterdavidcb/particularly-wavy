﻿Shader "Unlit/Particle_AdditiveSimple"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Tex("Texture", 2D) = "white" {}
		_Tex2("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (0.500000,0.500000,0.500000,1.00000)
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" } //"Queue" = "Transparent" }
		LOD 100
		Blend SrcAlpha One
		ZWrite Off
		Cull Back

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			sampler2D _MainTex;
			sampler2D _Tex;
			float4 _Tex_ST;
			sampler2D _Tex2;
			float4 _Tex2_ST;
			float4 _MainTex_ST;
			fixed4 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex) *TRANSFORM_TEX(v.uv, _Tex);
				o.color = v.color*_Color;
				/*if (o.color.a > 0.3)
				{
					o.color.a = 1;
				}*/
				//o.color.a *= 10;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) * i.color * _Color * tex2D(_Tex,i.uv)* tex2D(_Tex2,i.uv);
				//col.a = tex2D(_MainTex, i.uv).a;
				return col;
			}
			ENDCG
		}
	}
}
