﻿Shader "Custom/HeatEffect" 
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Texture",2D) = "white"{}
		_MaxDistance("Effect Size", float) = 1
		_Position("Collision", Vector) = (-1, -1, -1, -1)
		_EffectTime("Effect Time (ms)", float) = 0
	}

		SubShader
	{
		Pass
		{
			Tags
			{
				"LightMode" = "ForwardBase"
			}
			CGPROGRAM

			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"

			float4 _Color;
			sampler2D _MainTex;
			float4 _Position;
			float _MaxDistance;
			float _EffectTime;
			float _Strength;

			struct Interpolators
			{
				float4 position : SV_POSITION;
				float2 uv: TEXCOORD0;
				float3 normal:TEXCOORD1;
				float3 worldPos:TEXCOORD2;
				float customDist:FLOAT; 
			};

			struct VertexData
			{
				float4 position:POSITION;
				float2 uv: TEXCOORD0;
				float3 normal:NORMAL;
			};

			Interpolators MyVertexProgram(VertexData v)
			{
				Interpolators i;
				i.position = UnityObjectToClipPos(v.position);
				_Position = UnityObjectToClipPos(_Position);
				i.worldPos = mul(unity_ObjectToWorld, v.position);
				i.customDist = distance(i.position, _Position);
				i.uv = v.uv;
				i.normal = normalize(UnityObjectToWorldNormal(v.normal));
				return i;
			}

			float4 MyFragmentProgram(Interpolators i) : SV_TARGET
			{
				i.normal = normalize(i.normal);
				float3 lightDir = _WorldSpaceLightPos0.xyz;
				float4 albedo;
				float viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
				float3 halfVector = normalize(lightDir + viewDir);
				float4 spec = pow(dot(halfVector, i.normal), 100);
				float sat = 2.5* saturate(dot(lightDir, i.normal));

				if (_EffectTime > 0)
				{
					if (i.customDist < _MaxDistance)
					{
						albedo = tex2D(_MainTex, i.uv) * _Color*(_EffectTime / 3) *sat  + _Color.a *(i.customDist / _MaxDistance);// *_Color.a*(_EffectTime / 3);// +_Color.a *(i.customDist / _MaxDistance)*(i.customDist / _MaxDistance)*(_EffectTime / 3);
					}
					else
					{
						albedo = tex2D(_MainTex, i.uv) * float4(1, 1, 1, 1) * sat;
					}
					
				}
				else
				{
					albedo = tex2D(_MainTex, i.uv) * float4(1, 1, 1, 1) * sat;// + spec;
				}
				//albedo = tex2D(_MainTex, i.uv) * float4(1, 1, 1, 1) * saturate(dot(lightDir, i.normal));
				return albedo;// +spec;
			}

			ENDCG
		}
	}
}
