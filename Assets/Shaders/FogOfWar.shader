﻿Shader "Custom/FogOfWar" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
		SubShader{
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			LOD 200
			Lighting Off
			//ZWrite Off

			Pass
		{

			CGPROGRAM
				// Physically based Standard lighting model, and enable shadows on all light types
				#pragma vertex vert_img
				#pragma fragment frag

				// Use shader model 3.0 target, to get nicer looking lighting
				#pragma target 3.0

				#include "UnityCG.cginc"

				uniform sampler2D _MainTex;

		


				fixed4 frag(v2f_img i) : SV_Target
				{
					return tex2D(_MainTex, i.uv);
					
				}
				ENDCG
			}
	}
	FallBack "Diffuse"
}
