﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/ThirdShader" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MinVisDistance ("Min View Distance", float) = 1
		_MaxVisDistance("Max View Distance", float) = 1
	}
	SubShader 
		{

			Pass
		{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc" 

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

				struct v2f
			{
				half4 pos       : POSITION;
				fixed4 color : COLOR0;
				half2 uv        : TEXCOORD0;
			};



			sampler2D   _MainTex;
			half        _MinVisDistance;
			half        _MaxVisDistance;



			v2f vert(appdata_full v)
			{
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy;
				o.color = v.color;

				//distance falloff
				half3 viewDirW = _WorldSpaceCameraPos - UnityObjectToClipPos(v.vertex);
				half viewDist = length(viewDirW);
				half falloff = saturate((viewDist - _MinVisDistance) / (_MaxVisDistance - _MinVisDistance));
				o.color.a *= (1.0f - falloff);
				return o;
			}


			fixed4 frag(v2f i) : COLOR
			{
				fixed4 color = tex2D(_MainTex, i.uv) * i.color;
			return color;
			}
			ENDCG
				}
		}
	FallBack "Diffuse"
}
