﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DatabaseTut
{
	[Serializable]
	public class LevelData : IIdentity
	{
		int levelIndex;
		string levelName;
		string description;
		List<ObjectData> moveableObjects;
		bool locked;
		Sprite image;

		public int ID
		{
			get
			{
				return levelIndex;
			}

			set
			{
				levelIndex = value;
			}
		}

		public bool Locked
		{
			get
			{
				return locked;
			}

			set
			{
				locked = value;
			}
		}

		public string Name
		{
			get
			{
				return levelName;
			}

			set
			{
				levelName = value;
			}
		}

		public string Description
		{
			get
			{
				return description;
			}

			set
			{
				description = value;
			}
		}

		public List<ObjectData> MoveableObjects
		{
			get
			{
				return moveableObjects;
			}
			set
			{
				moveableObjects = value;
			}
		}

		public void ShowEditFields()
		{
			throw new NotImplementedException();
		}
	}
}

