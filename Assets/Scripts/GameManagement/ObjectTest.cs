﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using DatabaseTut;


public class ObjectTest : MonoBehaviour
{

	List<GameObject> stuff;
	// Use this for initialization
	void Start()
	{
		Game.Load();
		if (Game.levels.IndexOf(Game.levels.Get(SceneManager.GetActiveScene().name)) == -1)
		{
			//Debug.Log("Adding level to list");
			stuff = new List<GameObject>();


			stuff = SceneManager.GetActiveScene().GetRootGameObjects().ToList();
			for (int j = stuff.Count - 1; j > -1; j--)
			{
				if (stuff[j].layer != 9)
				{
					//Debug.Log("Removing");
					stuff.Remove(stuff[j]);
				}

			}

			//Debug.Log(stuff.Count);

			List<ObjectData> oData = new List<ObjectData>();
			for (int j = 0; j < stuff.Count; j++)
			{
				ObjectData temp = new ObjectData();
				temp.objectName = stuff[j].name;
				temp.op = new ObjectPosition();
				temp.op.px = stuff[j].transform.position.x;
				temp.op.py = stuff[j].transform.position.y;
				temp.op.pz = stuff[j].transform.position.z;
				temp.op.rw = stuff[j].transform.rotation.w;
				temp.op.rx = stuff[j].transform.rotation.x;
				temp.op.ry = stuff[j].transform.rotation.y;
				temp.op.rz = stuff[j].transform.rotation.z;
				oData.Add(temp);
			}
			//Debug.Log("count " + oData.Count);
			//Game.Load();

			LevelData thisLevel = new LevelData();
			thisLevel.ID = (int)(SceneManager.GetActiveScene().name[SceneManager.GetActiveScene().name.Length - 1]-'0');
			thisLevel.Locked = thisLevel.ID == 1 ? false : true;
			thisLevel.Name = SceneManager.GetActiveScene().name;
			thisLevel.MoveableObjects = oData;
			Game.levels.Add(thisLevel);
			Game.Save();
		}
		else
		{
			//Debug.Log("Already contains index");
			LevelData level = Game.levels.Get(SceneManager.GetActiveScene().name);
			stuff = SceneManager.GetActiveScene().GetRootGameObjects().ToList();
			for (int j = stuff.Count - 1; j > -1; j--)
			{
				if (stuff[j].layer != 9)
				{
					//Debug.Log("Removing");
					stuff.Remove(stuff[j]);
				}

			}
			//Debug.Log(stuff.Count);
			List<ObjectData> oData = new List<ObjectData>();
			for (int j = 0; j < stuff.Count; j++)
			{
				
				ObjectData temp = new ObjectData();
				temp.objectName = stuff[j].name;
				temp.op = new ObjectPosition();
				temp.op.px = stuff[j].transform.position.x;
				temp.op.py = stuff[j].transform.position.y;
				temp.op.pz = stuff[j].transform.position.z;
				temp.op.rw = stuff[j].transform.rotation.w;
				temp.op.rx = stuff[j].transform.rotation.x;
				temp.op.ry = stuff[j].transform.rotation.y;
				temp.op.rz = stuff[j].transform.rotation.z;
				oData.Add(temp);
			}

			//update the database
			bool found = false;
			for (int i = 0; i < oData.Count; i++)
			{
				found = false;
				if (level.MoveableObjects.Count > 0)
				{
					for (int j = 0; j < level.MoveableObjects.Count; j++)
					{
						if (oData[i].objectName == level.MoveableObjects[j].objectName)
						{
							found = true;
							level.MoveableObjects[j] = oData[i];
						}
						else if (j == level.MoveableObjects.Count - 1 && !found)
						{
							//Debug.Log("Adding new object");
							level.MoveableObjects.Add(oData[i]);
						}
					}
				}
				else
				{
					level.MoveableObjects.Add(oData[i]);
				}
				
			}
			//Debug.Log("Saving xml");
			Game.Save();
		}
		

	}

	// Update is called once per frame
	void Update()
	{

	}
}
