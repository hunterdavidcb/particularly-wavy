﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameSaveData
{
	public List<int> unlockedLevels;
	public List<PowerLevels> powerLevels; //use this to store power use for each attempt
	public List<StaminaLevels> staminaLevels;
	public List<EleganceLevels> eleganceLevels;
	public float levelTime;
	public string screenshotPath;

	public GameSaveData()
	{
		unlockedLevels = new List<int>();
		powerLevels = new List<PowerLevels>();
		staminaLevels = new List<StaminaLevels>();
		eleganceLevels = new List<EleganceLevels>();
		levelTime = float.MaxValue;
	}
}

[Serializable]
public class PowerLevels:IEquatable<PowerLevels>
{
	public int LevelIndex;
	public float PowerLevel;

	public PowerLevels()
	{
		LevelIndex = -1;
		PowerLevel = -1f;
	}

	public bool Equals(PowerLevels other)
	{
		return LevelIndex == other.LevelIndex;
	}
}

[Serializable]
public class StaminaLevels:IEquatable<StaminaLevels>
{
	public int LevelIndex;
	public float StaminaLevel;

	public StaminaLevels()
	{
		LevelIndex = -1;
		StaminaLevel = -1f;
	}

	public bool Equals(StaminaLevels other)
	{
		return LevelIndex == other.LevelIndex;
	}
}

[Serializable]
public class EleganceLevels : IEquatable<EleganceLevels>
{
	public int LevelIndex;
	public int EleganceLevel;

	public EleganceLevels()
	{
		LevelIndex = -1;
		EleganceLevel = -1;
	}

	public bool Equals(EleganceLevels other)
	{
		return LevelIndex == other.LevelIndex;
	}
}
