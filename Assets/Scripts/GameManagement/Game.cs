﻿using System.IO;
using UnityEngine;

namespace DatabaseTut
{
	public static class Game
	{

		//public static readonly string ACTOR_PATH = Path.Combine(Application.dataPath + "/data" , "actors.xml");
		//public static readonly string ARMOR_PATH = Path.Combine(Application.dataPath + "/data", "armors.xml");
		//public static readonly string WEAPON_PATH = Path.Combine(Application.dataPath + "/data", "weapons.xml");
		//public static readonly string LEVEL_PATH = Application.dataPath + "/data/" + "levels.xml";
		public static readonly string LEVEL_PATH = Application.dataPath + "/Resources/" + "levels.xml";
		public static readonly string ProgressFile = "PlayerProgress";
		public static GameSaveData gameData;

		//public static Database<ActorData> actors;
		//public static Database<ArmorData> armors;
		//public static Database<WeaponData> weapons;
		public static Database<LevelData> levels;

		public static void Save()
		{
			//actors.Save<Database<ActorData>>(ACTOR_PATH);
			//armors.Save<Database<ArmorData>>(ARMOR_PATH);
			//weapons.Save<Database<WeaponData>>(WEAPON_PATH);
			levels.Save<Database<LevelData>>(LEVEL_PATH);
		}

		public static void Load()
		{
			//actors = Database<ActorData>.Load<Database<ActorData>>(ACTOR_PATH);
			//armors = Database<ArmorData>.Load<Database<ArmorData>>(ARMOR_PATH);
			//weapons = Database<WeaponData>.Load<Database<WeaponData>>(WEAPON_PATH);
			//Debug.Log(LEVEL_PATH);
			levels = Database<LevelData>.Load<Database<LevelData>>(LEVEL_PATH);
		}
	}
}


