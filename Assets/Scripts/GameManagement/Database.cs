﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace DatabaseTut
{
	[XmlRoot("Database")]
	public class Database<T> where T: IIdentity
	{

		private List<T> _elements;


		[XmlArray("Elements"),XmlArrayItem("Element")]
		public List<T> Elements { get { return _elements; } set { _elements = value; } }

		public int Length { get { return _elements.Count; } }

		public Database()
		{
			_elements = new List<T>();
		}

		public void Add(T element)
		{
			if (!Contains(element))
			{
				element.ID = _elements.Count +1;
				_elements.Add(element);
			}
			
		}

		public void Remove(T element)
		{
			_elements.Remove(element);
		}

		#region Gets
		public T Get(int id)
		{
			int i = FindElement(id);
			if (i >= 0)
			{
				return _elements[i];
			}
			return default(T);
		}

		public T Get(string name)
		{
			int i = FindElement(name);
			if (i >= 0)
			{
				return _elements[i];
			}
			return default(T);

		}

		public T GetAt(int index)
		{
			if (index >= 0 && index < _elements.Count)
			{
				return _elements[index];
			}
			return default(T);

		}

		#endregion

		#region Removes
		public void Remove(int id)
		{
			int i = FindElement(id);
			if (i >= 0)
			{
				_elements.RemoveAt(i);
				UpdateIDs();
			}
		}

		public void Remove(string name)
		{
			int i = FindElement(name);
			if (i >= 0)
			{
				_elements.RemoveAt(i);
				UpdateIDs();
			}
			
		}

		public void RemoveAt(int index)
		{
			_elements.RemoveAt(index);
			UpdateIDs();
		}

		private void UpdateIDs()
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				T item = _elements[i];
				item.ID = i + 1;
				_elements[i] = item;
			}
		}

		#endregion

		public void Replace(int index, T element)
		{
			if (index >= 0 && index < _elements.Count)
			{
				_elements[index] = element;
			}

			Add(element);
		}

		public void Clear()
		{
			_elements.Clear();
		}

		private int DetermineID()
		{
			int id = 0;
			for (int i = 0; i < _elements.Count; i++)
			{
				if (_elements[i].ID > id)
				{
					id = _elements[i].ID;
				}
			}
			return id+1;
		}

		public bool Contains(T element)
		{
			return FindElement(element.Name) >= 0;
		}

		#region IndexOf

		public int IndexOf(T element)
		{
			if (element != null)
			{
				return FindElement(element.ID);
			}
			return -1;
			
		}

		public int IndexOf(int id)
		{
			return FindElement(id);
		}

		public int IndexOf(string name)
		{
			return FindElement(name);
		}

		#endregion

		private int FindElement(int id)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				if (_elements[i].ID == id)
				{
					return i;
				}
			}
			return -1;
		}

		private int FindElement(string name)
		{
			for (int i = 0; i < _elements.Count; i++)
			{
				if (_elements[i].Name == name)
				{
					return i;
				}
			}
			return -1;
		}

		public void Save<U>(string path) where U : Database<T>
		{
			var serializer = new XmlSerializer(typeof(U));
			using (var stream = new FileStream(path, FileMode.Create))
			{
				serializer.Serialize(stream, this);
			}
		}

		public static U Load<U>(string path) where U : Database<T>, new()
		{
			//Resources.Load(path);
			//Debug.Log("Loading: " + path);
			string withoutExtension = Path.GetFileNameWithoutExtension(path);
			TextAsset ta = Resources.Load(withoutExtension) as TextAsset;
			var serializer = new XmlSerializer(typeof(U));
			using (var reader = new System.IO.StringReader(ta.text))
			{
				return serializer.Deserialize(reader) as U;
			}
				//XmlDocument xmlDoc = new XmlDocument();
				//if (ta == null)
				//{
				//	Debug.Log("TA is null");
				//}
			//return ta;
			//xmlDoc.LoadXml(ta.text);
			//xmlDoc
			//XmlReader reader = new XmlReader(xmlDoc);
			
			//serializer.Deserialize()
			//XmlReader reader = XmlReader.Create()
			//return xmlDoc as U;
			//serializer.Deserialize()
			//using (var stream = new FileStream(path, FileMode.Open))
			//{
			//	return serializer.Deserialize(stream) as U;
			//}
			//if (File.Exists(path))
			//{
			//	var serializer = new XmlSerializer(typeof(U));
			//	using (var stream = new FileStream(path, FileMode.Open))
			//	{
			//		 return serializer.Deserialize(stream) as U;
			//	}
			//}

			//return new U();
		}
	}
}

