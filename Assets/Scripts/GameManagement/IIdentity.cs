﻿namespace DatabaseTut
{
	public interface IIdentity : IEditable
	{
		int ID { get; set; }
		string Name { get; set; }
		string Description { get; set; }
	}
}