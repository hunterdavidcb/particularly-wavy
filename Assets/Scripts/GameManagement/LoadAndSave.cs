﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.Linq;

public static class LoadAndSave 
{
	public static void Save(string fileName, GameSaveData data)
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file;
		if (File.Exists(fileName+".dat"))
		{
			file = File.Open(Application.persistentDataPath + "/" + fileName + ".dat",FileMode.Open);
		}
		else
		{
			file = File.Create(Application.persistentDataPath + "/" + fileName + ".dat");
		}

		//Debug.Log("Saving file");
		//Debug.Log(Application.persistentDataPath);
		bf.Serialize(file, data);
		file.Close();
	}

	public static GameSaveData Load(string fileName)
	{
		if (File.Exists(Application.persistentDataPath + "/" + fileName + ".dat"))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/" + fileName + ".dat", FileMode.Open);
			GameSaveData data = (GameSaveData)bf.Deserialize(file);

			file.Close();
			return data;
		}
		return new GameSaveData();
	}

	public static List<string> RefreshFiles()
	{
		List<string> files = new List<string>();
		files = Directory.GetFiles(Application.persistentDataPath , "*.*", SearchOption.AllDirectories).
			Where(s => s.EndsWith(".dat", StringComparison.OrdinalIgnoreCase)).ToList();
		return files;
	}

	public static Sprite GetImage(string fileName)
	{
		Texture2D tex;
		byte[] fileData;
		//Debug.Log(fileName);
		if (File.Exists(fileName))
		{
			//Debug.Log("It exists");
			fileData = File.ReadAllBytes(fileName);
			tex = new Texture2D(2, 2);
			tex.LoadImage(fileData);
			Sprite sprite = new Sprite();
			sprite = Sprite.Create(tex, new Rect(0,0,tex.width,tex.height), new Vector2(1,1), 100f);
			return sprite;
		}
		else
		{
			return new Sprite();
		}
	}

	public static void CleanScreenshots()
	{
		List<string> files = new List<string>();
		files = Directory.GetFiles(Application.persistentDataPath , "*.*", SearchOption.AllDirectories).
			Where(s => s.EndsWith(".dat", StringComparison.OrdinalIgnoreCase)).ToList();
		List<string> screenshots = new List<string>();
		//screenshots = Directory.GetFiles(Application.persistentDataPath + "/Screenshots", "*.*").ToList();
		foreach (var file in files)
		{
			GameSaveData d = LoadInternal(file);
			screenshots.Add(d.screenshotPath);
			//Debug.Log(d.screenshotPath);
		}

		//Debug.Log(screenshots.Count);
		List<string> unusedScreenhots = new List<string>();
		unusedScreenhots = Directory.GetFiles(Application.persistentDataPath + "/Screenshots/").ToList();
		foreach (var screenshot in unusedScreenhots)
		{
			//Debug.Log("Checking " + screenshot);
			if (!screenshots.Contains(screenshot))
			{
				//Debug.Log("Master list does not contain " + screenshot);
				File.Delete(screenshot);
			}
		}
	}

	private static GameSaveData LoadInternal(string s)
	{
		if (File.Exists(s))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(s, FileMode.Open);
			GameSaveData data = (GameSaveData)bf.Deserialize(file);

			//set variables from data
			//localhealth = data.health
			file.Close();
			return data;
		}
		return new GameSaveData();
	}
}
