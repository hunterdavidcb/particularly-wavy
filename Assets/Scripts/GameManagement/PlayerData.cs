﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class ObjectPosition
{
	public float px,py,pz; // for storing the vector3 data
	public float rx, ry, rz, rw; //for storing the rotation data
}

[Serializable]
public class ObjectData
{
	public ObjectPosition op; //for storing the object's location
	public string objectName; //for finding the object in the scene and updating it
	//public string[] reflectedMaterials; //for storing and loading the materials from Resources
	//public string lasterMaterial; //for storing and loading laser material from Resources
	//public string[] targetMaterials;
}
