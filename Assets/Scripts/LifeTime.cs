﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTime : MonoBehaviour 
{
	float timerMax = 0.2f;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		timerMax -= Time.deltaTime;

		if (timerMax < 0)
		{
			Destroy(gameObject);
		}
	}
}
