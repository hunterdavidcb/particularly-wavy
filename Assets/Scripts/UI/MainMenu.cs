﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DatabaseTut;
using UnityEngine.UI;
using System.Linq;

public class MainMenu : MonoBehaviour
{
	public GameObject playButton;
	public GameObject quitButton;
	public GameObject loadLevelButton;
	public GameObject optionButton;
	public GameObject loadPanel;
	public GameObject optionsPanel;
	public GameObject filePrefab;
	public GameObject loadPanelContent;
	public Text powerLevelText;
	List<GameObject> listOfPrefabs;

	public Music musicManager;

	[FMODUnity.EventRef]
	public string pressed;
	
	//List<LevelData> levels;
	int choice = -1;

	// Use this for initialization
	void Start ()
	{

		//FMOD_StudioEventEmitter[] ms = FindObjectsOfType<FMOD_StudioEventEmitter>();

		Music[] musicManagers = FindObjectsOfType<Music>();
		for (int i = 0; i < musicManagers.Length; i++)
		{
			if (musicManagers[i] != musicManager)
			{
		//		//FMODUnity.RuntimeManager.DetachInstanceFromGameObject(FMOD.e)
				Destroy(musicManagers[i].gameObject);
			}
		}
		listOfPrefabs = new List<GameObject>();
		if (Game.gameData == null)
		{
			Game.gameData = LoadAndSave.Load(Game.ProgressFile);
			if (Game.gameData == null)
			{
				Debug.Log("Creating new gamedata");
				Game.gameData = new GameSaveData();
				//LoadAndSave.Save("PlayerProgress", gameData);
				LoadAndSave.Save(Game.ProgressFile, Game.gameData);
			}
			else
			{
				Debug.Log("Loaded successfully");
			}

			if (!Game.gameData.unlockedLevels.Contains(1))
			{
				Debug.Log("Adding key");
				Game.gameData.unlockedLevels.Add(1);
			}
			//Debug.Log(Game.gameData.unlockedLevels.Count);
			
		}
		else
		{
			Game.gameData = LoadAndSave.Load(Game.ProgressFile);
		}


			Game.Load();

		if (Game.levels == null)
		{
			Debug.Log("Levels is null");
		}
		else
		{
			
			//puts unlocked levels in proper order, in case of strangeness from in editor testing
			Game.gameData.unlockedLevels.Sort();
		}



		if (Game.gameData.unlockedLevels.Count > 1)
		{
			playButton.GetComponentInChildren<Text>().text = "Continue";
		}
		
	}

	void ButtonListener(string s)
	{

		//Text text;
		int index = -1;
		for (int i = 0; i < listOfPrefabs.Count; i++)
		{
			if (listOfPrefabs[i].GetComponentInChildren<Text>().text == s)
			{
				index = i;
			}
			listOfPrefabs[i].GetComponent<Button>().image.color = new Color(1f, 1f, 1f);
		}

		PowerLevels pl = new PowerLevels();
		StaminaLevels sl = new StaminaLevels();
		EleganceLevels el = new EleganceLevels();
		el.LevelIndex = sl.LevelIndex = pl.LevelIndex = Game.levels.Get(s).ID;
		if (Game.gameData.powerLevels.Contains(pl))
		{
			powerLevelText.gameObject.SetActive(true);
			powerLevelText.text = "Used " + Game.gameData.powerLevels[Game.gameData.powerLevels.IndexOf(pl)].PowerLevel + " Watts \n"
				+ "Used " + Game.gameData.staminaLevels[Game.gameData.staminaLevels.IndexOf(sl)].StaminaLevel + " Stamina \n"
				+"Completed in " + Game.gameData.eleganceLevels[Game.gameData.eleganceLevels.IndexOf(el)].EleganceLevel + " Rays of Light \n"
				+ "Completed in " + Game.gameData.levelTime + " Seconds";
		}
		
		listOfPrefabs[index].GetComponent<Button>().image.color = new Color(0f, 0.7f, .1f);
		choice = Game.levels.Get(s).ID;
	}

	// Update is called once per frame
	void Update ()
	{


	}

	public void Play()
	{
		//if they've played past the first level
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		if (Game.gameData.unlockedLevels.Count > 1)
		{
			Game.gameData.unlockedLevels.Sort();
			//load the scene with that key
			Load(Game.gameData.unlockedLevels.Count-1);
		}
		else
		{
			Load(1);
		}
	}

	public void Load()
	{
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		if (choice != -1)
		{
			//validate levelIndex
			Load(choice);
		}
	}

	public void OpenOptions()
	{
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		loadPanel.SetActive(true);
		playButton.SetActive(false);
		loadLevelButton.SetActive(false);
		quitButton.SetActive(false);
		optionButton.SetActive(false);
		optionsPanel.SetActive(true);
	}

	private void Load(int levelIndex)
	{
		//check that index is valid
		if (levelIndex > 0 && levelIndex < SceneManager.sceneCountInBuildSettings)
		{
			//SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
			SceneManager.LoadSceneAsync(levelIndex);
		}
	}


	public void OpenLoadPanel()
	{
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		loadPanel.SetActive(true);
		playButton.SetActive(false);
		loadLevelButton.SetActive(false);
		quitButton.SetActive(false);
		optionButton.SetActive(false);


		foreach (var level in Game.gameData.unlockedLevels)
		{
			//Debug.Log("Spawning buttons");
			//Debug.Log(level);
			GameObject go = Instantiate(filePrefab, loadPanelContent.transform);
			go.GetComponentInChildren<Text>().text = Game.levels.Get(level).Name;
			go.GetComponent<Button>().onClick.AddListener(() => ButtonListener(Game.levels.Get(level).Name));
			listOfPrefabs.Add(go);
		}
		//foreach (string file in saveFiles)
		//{
			
			//string[] s = file.Split('/');
			//string fileNamePlusExt = s[s.Length-1];
			//string fileName = fileNamePlusExt.Remove(fileNamePlusExt.IndexOf('.'));
			//string fileName = Path.GetFileNameWithoutExtension(file);
		//}
	}

	public void CloseLoadPanel()
	{
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		loadPanel.SetActive(false);
		playButton.SetActive(true);
		loadLevelButton.SetActive(true);
		quitButton.SetActive(true);
		choice = -1;
		powerLevelText.gameObject.SetActive(false);
		ClearList();
	}

	void ClearList()
	{
		foreach (var item in listOfPrefabs)
		{
			Destroy(item);
		}
		listOfPrefabs.Clear();
	}

	public void CloseOptions()
	{
		optionsPanel.SetActive(false);

		FMODUnity.RuntimeManager.PlayOneShot(pressed);

		playButton.SetActive(true);
		loadLevelButton.SetActive(true);
		quitButton.SetActive(true);
		optionButton.SetActive(true);
	}

	public void Quit()
	{
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		Application.Quit();
	}

}
