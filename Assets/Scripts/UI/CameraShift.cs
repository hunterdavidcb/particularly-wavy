﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShift : MonoBehaviour 
{
	float speed = 0.01f;
	// Use this for initialization
	void Start () 
	{
		Target[] ts = FindObjectsOfType<Target>();
		foreach (var item in ts)
		{
			item.TargetReached += OnTargetReached;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	protected void OnTargetReached(GameObject go, int maxDepth)
	{
		StartCoroutine(Shift(go));
	}

	IEnumerator Shift(GameObject go)
	{
		Vector3 displaced = new Vector3(go.transform.position.x, go.transform.position.y, transform.position.z);
		float dist = Vector3.Distance(transform.position, displaced);
		while (dist > .1)
		{
			//Debug.Log("Changing");
			Vector3 pos = Vector3.Lerp(transform.position, displaced, speed*Time.deltaTime);
			transform.position = pos;
			dist = Vector3.Distance(transform.position, displaced);
			yield return null;
		}
		

	}
}
