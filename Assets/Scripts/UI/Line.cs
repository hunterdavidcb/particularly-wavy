﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour 
{
	public List<Vector3> points;

	List<Vector3> meshPoints;
	float width = 0.125f;
	// Use this for initialization
	void Start () 
	{
		meshPoints = new List<Vector3>();
		Vector3 left, right,dir;
		float xPerp, yPerp;
		GameObject caret = new GameObject();
		if (points != null )
		{
			for (int i = 0; i < points.Count-1; i++)
			{
				caret.transform.position = points[i];
				//Debug.Log("original rotation " + caret.transform.rotation.eulerAngles);
				//caret.transform.LookAt(points[i+1]);
				dir = (points[i + 1] - points[i]).normalized;
				xPerp = dir.x * Mathf.Cos(90 * Mathf.Deg2Rad) - dir.y * Mathf.Sin(90 * Mathf.Deg2Rad);
				yPerp = dir.x * Mathf.Sin(90 * Mathf.Deg2Rad) + dir.y + Mathf.Cos(90 * Mathf.Deg2Rad);

				left = new Vector3(points[i].x + xPerp * width, points[i].y + yPerp * width, 0f);
				right = new Vector3(points[i].x - xPerp * width, points[i].y - yPerp * width, 0f);
				//caret.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y,dir.x));
				//Debug.Log("rotated rotation " + caret.transform.rotation.eulerAngles);


				//	right = caret.transform.position + caret.transform.right * width;
				//	left = caret.transform.position - caret.transform.right * width;

				Debug.Log("left " + left);
				Debug.Log("right " + right);
				if (i / 2 == 0)
				{
					Debug.Log("Adding left first");
					meshPoints.Add(left);
					meshPoints.Add(right);
				}
				else
				{
					Debug.Log("Adding right first");
					meshPoints.Add(right);
					meshPoints.Add(left);
				}
				
			}

			caret.transform.position = points[points.Count-1];
			dir = (points[points.Count - 2] - points[points.Count - 1]).normalized;
			xPerp = dir.x * Mathf.Cos(90 * Mathf.Deg2Rad) - dir.y * Mathf.Sin(90 * Mathf.Deg2Rad);
			yPerp = dir.x * Mathf.Sin(90 * Mathf.Deg2Rad) + dir.y + Mathf.Cos(90 * Mathf.Deg2Rad);

			left = new Vector3(points[points.Count - 1].x + xPerp * width, points[points.Count - 1].y + yPerp * width, 0f);
			right = new Vector3(points[points.Count - 1].x - xPerp * width, points[points.Count - 1].y - yPerp * width, 0f);
			//caret.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y, dir.x));
			//if (dir.y < 0.1)
			//{
			//	left = caret.transform.position + caret.transform.up * width;
			//	right = caret.transform.position - caret.transform.up * width;
			//	Debug.Log("y diff is small");
			//}
			//else
			//{
			//	right = caret.transform.position - caret.transform.right * width;
			//	left = caret.transform.position + caret.transform.right * width;
			//}

			//caret.transform.LookAt(points[points.Count-2]);

			//these need to be added backwards, first right then left, since we are facing the opposite way
			Debug.Log("Adding right first");
			meshPoints.Add(right);
			meshPoints.Add(left);
			Debug.Log("left " + left);
			Debug.Log("right " + right);
			Destroy(caret);
			DrawMesh();
		}
		//transform.rotation = Quaternion.Euler(0, -90f, 0);
	}

	private void DrawMesh()
	{
		Vector3[] verticies = new Vector3[meshPoints.Count];

		for (int i = 0; i < verticies.Length; i++)
		{
			verticies[i] = meshPoints[i];
		}

		int[] triangles = new int[((meshPoints.Count / 2) - 1) * 6];

		Debug.Log("number of tris " + triangles.Length);

		//Works on linear patterns tn = bn+c
		int position = 6;
		for (int i = 0; i < (triangles.Length / 6); i++)
		{
			triangles[i * position] = 2 * i;
			triangles[i * position + 3] = 2 * i;

			triangles[i * position + 1] = 2 * i + 3;
			triangles[i * position + 4] = (2 * i + 3) - 1;

			triangles[i * position + 2] = 2 * i + 1;
			triangles[i * position + 5] = (2 * i + 1) + 2;
		}


		Mesh mesh = GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		mesh.vertices = verticies;
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
	}

	// Update is called once per frame
	void Update () 
	{
		
	}
}
