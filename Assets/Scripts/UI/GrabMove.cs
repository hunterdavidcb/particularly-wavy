﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

//[ExecuteInEditMode]
public class GrabMove : MonoBehaviour
{
	private Vector3 previous;
	private Vector3 screenPoint;
	private Vector3 offset;
	//private Vector3 objectPosition;

	private Vector3 _mouseReference;
	private float _rotation;
	private bool _isRotating;
	Vector3 startDragDir;
	float startingAngle;

	public bool canRotate;
	public bool canMove;
	[SerializeField]
	int CurrentSegment = -1;

	public List<Vector3> p;

	public float minRotation;
	public float maxRotation;

	public MoveType movementType = MoveType.Both;
	public RotateType rotationType = RotateType.ThreeHundredSixy;

	public enum MoveType
	{
		X_Only,
		Y_Only,
		Both
	}

	public enum RotateType
	{
		Ninety,
		OneHundredEighty,
		ThreeHundredSixy,
		MinMax
	}

	public struct Segment
	{
		public Vector2 min, max, dir;
	}

	List<Segment> segments;

	public delegate void ObjectMoveHandler();
	public event ObjectMoveHandler ObjectMoved;

	private void Awake()
	{
		segments = new List<Segment>();
		if (canMove || canRotate)
		{
			//change layer to moveableobject layer
			gameObject.layer = 9;
		}

		if (transform.parent != null)
		{
			if (transform.parent.GetComponent<LinePolygonRenderer>() != null)
			{
				p = transform.parent.GetComponent<LinePolygonRenderer>().points;
				for (int i = 0; i < p.Count - 1; i++)
				{
					Segment s = new Segment();
					if (p[i].x > p[i + 1].x || p[i].y > p[i + 1].y)
					{

						s.min = new Vector2(p[i + 1].x, p[i + 1].y);
						s.max = new Vector2(p[i].x, p[i].y);
					}
					else if (p[i].x < p[i + 1].x || p[i].y < p[i + 1].y)
					{
						s.min = new Vector2(p[i].x, p[i].y);
						s.max = new Vector2(p[i + 1].x, p[i + 1].y);
					}
					s.dir = (p[i + 1] - p[i]).normalized;
					if (Mathf.Approximately(s.dir.x, 0f))
					{
						s.dir.x = 0f;
					}
					if (Mathf.Approximately(s.dir.y, 0f))
					{
						s.dir.y = 0f;
					}
					segments.Add(s);
				}
				for (int i = 0; i < segments.Count; i++)
				{
					
					if ((transform.position.x >= segments[i].min.x && transform.position.y >= segments[i].min.y
						&& transform.position.x <= segments[i].max.x && transform.position.y <= segments[i].max.y))
					{
						CurrentSegment = i;
						break;
					}
				}

				
			}
			else
			{
				Debug.Log("polygon renderer is null");
				p = new List<Vector3>();
			}
		}
		else
		{
			Debug.Log(gameObject.name + "'s parent is null");
			p = new List<Vector3>();
		}

		if (canRotate && (rotationType == RotateType.MinMax || rotationType == RotateType.OneHundredEighty || rotationType == RotateType.Ninety))
		{
			switch (rotationType)
			{
				case RotateType.Ninety:
					minRotation = transform.rotation.eulerAngles.z - 45f;
					maxRotation = transform.rotation.eulerAngles.z + 45f;
					break;
				case RotateType.OneHundredEighty:
					minRotation = transform.rotation.eulerAngles.z - 90f;
					maxRotation = transform.rotation.eulerAngles.z + 90f;
					break;
				case RotateType.MinMax:
					break;
				default:
					break;
			}
		}

	}
	// Use this for initialization
	void Start()
	{



		//_rotation = Vector3.zero;
		if (GetComponent<MeshCollider>() != null)
		{
			if (GetComponent<MeshCollider>().sharedMesh == null)
			{
				GetComponent<MeshCollider>().sharedMesh = transform.GetChild(0).GetComponent<MeshCollider>().sharedMesh;
			}
		}

		Laser[] lasers = FindObjectsOfType<Laser>();
		for (int i = 0; i < lasers.Length; i++)
		{
			lasers[i].LaserActivated += OnLaserActivated;
			lasers[i].LaserDeactivated += OnLaserDeactivated;
		}


	}

	protected void OnLaserActivated()
	{

	}


	protected void OnLaserDeactivated()
	{

	}


	// Update is called once per frame
	void Update()
	{
		if (canRotate)
		{
			if (_isRotating)
			{

				Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
				pos = Input.mousePosition - pos;
				_rotation = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - startingAngle;
				if (rotationType == RotateType.MinMax || rotationType == RotateType.Ninety || rotationType == RotateType.OneHundredEighty)
				{
					_rotation = Mathf.Clamp(_rotation, minRotation, maxRotation);
				}
				transform.rotation = Quaternion.AngleAxis(_rotation, Vector3.forward);


				if (previous != Input.mousePosition)
				{
					previous = Input.mousePosition;
					OnObjectMoved();
				}


				// store mouse
				_mouseReference = Input.mousePosition;
			}

			if (Input.GetMouseButtonDown(1))
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast(ray, out hit))
				{
					if (hit.transform == transform)
					{
						OnRightMouseDown();
					}
				}

			}

			if (Input.GetMouseButtonUp(1))
			{
				OnRightMouseUp();
			}
		}

	}

	void OnRightMouseDown()
	{
		// rotating flag
		_isRotating = true;

		// store mouse
		startDragDir = Camera.main.WorldToScreenPoint(transform.position);
		startDragDir = Input.mousePosition - startDragDir;
		startingAngle = Mathf.Atan2(startDragDir.y, startDragDir.x) * Mathf.Rad2Deg;
		startingAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;


	}

	void OnRightMouseUp()
	{
		// rotating flag
		_isRotating = false;
		previous = Vector3.zero;
	}

	public void OnMouseDown()
	{
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(
			new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

		previous = Input.mousePosition;
	}

	public void OnMouseUp()
	{
		previous = Vector3.zero;
	}


	public void OnMouseDrag()
	{
		Vector3 curScreenPoint;
		Vector3 curPosition = new Vector3();
		if (canMove)
		{
			switch (movementType)
			{
				case MoveType.X_Only:
					curScreenPoint = new Vector3(Input.mousePosition.x, screenPoint.y, screenPoint.z);
					if (segments.Count >= 1)
					{
						if (segments[CurrentSegment].dir.x > 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.x,0f))
						{
							if (Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									
									if (!Mathf.Approximately(segments[CurrentSegment+1].dir.x,0) )
									{
										//Debug.Log("Increasing");
										CurrentSegment++;
									}
									
								}
							}
							else if (Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment > 0)
								{
									if (!Mathf.Approximately(segments[CurrentSegment - 1].dir.x, 0))
									{
										//Debug.Log("Decreasing");
										CurrentSegment--;
									}
									
								}
							}
						}
						else if (segments[CurrentSegment].dir.x < 0)
						{
							if (Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									if (!Mathf.Approximately(segments[CurrentSegment + 1].dir.x, 0))
									{
										//Debug.Log("Increasing");
										CurrentSegment++;
									}
								}
							}
							else if (Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment > 0)
								{
									if (!Mathf.Approximately(segments[CurrentSegment - 1].dir.x, 0))
									{
										//Debug.Log("Decreasing");
										CurrentSegment--;
									}
								}
							}
						}
					
						curScreenPoint.x = Mathf.Clamp(curScreenPoint.x, Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x,
						Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x);

					}

					offset.y = 0;
					curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);// + offset;
					break;
				case MoveType.Y_Only:
					curScreenPoint = new Vector3(screenPoint.x, Input.mousePosition.y, screenPoint.z);
					if (segments.Count >= 1)
					{
						if (segments[CurrentSegment].dir.y > 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.y, 0f))
						{
							if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y)
							{
								if (CurrentSegment < segments.Count - 1)
								{

									if (!Mathf.Approximately(segments[CurrentSegment + 1].dir.y, 0))
									{
										//Debug.Log("Increasing");
										CurrentSegment++;
									}

								}
							}
							else if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y)
							{
								if (CurrentSegment > 0)
								{
									if (!Mathf.Approximately(segments[CurrentSegment - 1].dir.y, 0))
									{
										//Debug.Log("Decreasing");
										CurrentSegment--;
									}

								}
							}
						}
						else if (segments[CurrentSegment].dir.y < 0)
						{
							if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									if (!Mathf.Approximately(segments[CurrentSegment + 1].dir.y, 0))
									{
										//Debug.Log("Increasing");
										CurrentSegment++;
									}
								}
							}
							else if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y)
							{
								if (CurrentSegment > 0)
								{
									if (!Mathf.Approximately(segments[CurrentSegment - 1].dir.y, 0))
									{
										//Debug.Log("Decreasing");
										CurrentSegment--;
									}
								}
							}
						}
						
						curScreenPoint.y = Mathf.Clamp(curScreenPoint.y, Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y,
						Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y);
					}

					offset.x = 0;
					curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);// + offset;
					break;
				case MoveType.Both:
					//Debug.Log("Both");
					curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

					if (segments.Count >= 1)
					{
						if (segments[CurrentSegment].dir.x > 0 &&
							segments[CurrentSegment].dir.y > 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.x, 0) &&
							!Mathf.Approximately(segments[CurrentSegment].dir.y, 0))
						{
							//x positive, y positive
							if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y &&
								Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
										//Debug.Log("Increasing");
										CurrentSegment++;
								}
							}
							else if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y &&
								Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment > 0)
								{
										//Debug.Log("Decreasing");
										CurrentSegment--;
								}
							}
						}
						else if (segments[CurrentSegment].dir.x < 0 &&
							segments[CurrentSegment].dir.y < 0)
						{
							//x negative, y negative
							if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y &&
								Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									//Debug.Log("Increasing");
									CurrentSegment++;
								}
							}
							else if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y &&
								Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment > 0)
								{
									//Debug.Log("Decreasing");
									CurrentSegment--;
								}
							}
						}
						else if (segments[CurrentSegment].dir.x < 0 &&
							segments[CurrentSegment].dir.y > 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.y, 0))
						{
							//x negative, y positive
							if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y &&
								Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									//Debug.Log("Increasing");
									CurrentSegment++;
								}
							}
							else if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y &&
								Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment > 0)
								{
									//Debug.Log("Decreasing");
									CurrentSegment--;
								}
							}
						}
						else if (segments[CurrentSegment].dir.x > 0 &&
							segments[CurrentSegment].dir.y < 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.y, 0))
						{
							//x positive, y negative
							if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y &&
								Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									//Debug.Log("Increasing");
									CurrentSegment++;
								}
							}
							else if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y &&
								Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment > 0)
								{
									//Debug.Log("Decreasing");
									CurrentSegment--;
								}
							}
						}
						else if (Mathf.Approximately(segments[CurrentSegment].dir.x, 0) &&
							segments[CurrentSegment].dir.y < 0)
						{
							//x zero, y negative
							if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y)
							{
								if (CurrentSegment < segments.Count - 1)
								{
										//Debug.Log("Increasing");
										CurrentSegment++;
								}
							}
							else if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y)
							{
								if (CurrentSegment > 0)
								{
										//Debug.Log("Decreasing");
										CurrentSegment--;
								}
							}
						}
						else if (Mathf.Approximately(segments[CurrentSegment].dir.x, 0) &&
							segments[CurrentSegment].dir.y > 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.y, 0))
						{
							//x zero, y positive
							if (Input.mousePosition.y > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									//Debug.Log("Increasing");
									CurrentSegment++;
								}
							}
							else if (Input.mousePosition.y < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y)
							{
								if (CurrentSegment > 0)
								{
									//Debug.Log("Decreasing");
									CurrentSegment--;
								}
							}
						}
						else if (Mathf.Approximately(segments[CurrentSegment].dir.y, 0) &&
							segments[CurrentSegment].dir.x < 0 )
						{
							//x negative, y zero
							if (Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									//Debug.Log("Increasing");
									CurrentSegment++;
								}
							}
							else if (Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment > 0)
								{
									//Debug.Log("Decreasing");
									CurrentSegment--;
								}
							}
						}
						else if (Mathf.Approximately(segments[CurrentSegment].dir.y, 0) &&
							segments[CurrentSegment].dir.x > 0 &&
							!Mathf.Approximately(segments[CurrentSegment].dir.x, 0))
						{
							//x positive, y zero
							if (Input.mousePosition.x > Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x)
							{
								if (CurrentSegment < segments.Count - 1)
								{
									//Debug.Log("Increasing");
									CurrentSegment++;
								}
							}
							else if (Input.mousePosition.x < Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x)
							{
								if (CurrentSegment > 0)
								{
									//Debug.Log("Decreasing");
									CurrentSegment--;
								}
							}
						}

						curScreenPoint = Camera.main.WorldToScreenPoint(LinePolygonRenderer.GetClosestPoint(Camera.main.ScreenToWorldPoint(curScreenPoint)
						, segments[CurrentSegment].min, segments[CurrentSegment].max));

						//are these two steps necessary?
						curScreenPoint.y = Mathf.Clamp(curScreenPoint.y, Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).y,
						Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).y);

						curScreenPoint.x = Mathf.Clamp(curScreenPoint.x, Camera.main.WorldToScreenPoint(segments[CurrentSegment].min).x,
						Camera.main.WorldToScreenPoint(segments[CurrentSegment].max).x);
					}

					
					
					curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);// + offset;
					break;
				default:
					break;
			}

			curPosition.y = Mathf.Clamp(curPosition.y, 0, float.MaxValue);
			//curPosition.z = 0f;
			transform.position = curPosition;
			if (previous != Input.mousePosition)
			{
				previous = Input.mousePosition;
				OnObjectMoved();
			}

		}

	}

	protected void OnObjectMoved()
	{
		if (ObjectMoved != null)
		{
			ObjectMoved();
		}
	}
}
