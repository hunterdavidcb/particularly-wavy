﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour 
{
	public bool isChecked;
	public Toggle tog;
	public RectTransform tutorialPanel;
	public Text tutText;
	string text = "Use space bar to turn the light on and off. \n The left mouse button moves objects. \n The right mouse button rotates objects.";
	// Use this for initialization
	void Start () 
	{
		isChecked = tog.isOn;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Switch()
	{
		isChecked = tog.isOn;
		if (isChecked)
		{
			ShowTutorial();
		}
		else
		{
			HideTutorial();
		}
	}

	void ShowTutorial()
	{
		tutorialPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 350f);
		tutorialPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 200f);
		tutText.gameObject.SetActive(true);
		tutText.rectTransform.position = new Vector3(0f, 0f, 0f);
		tutText.rectTransform.pivot = new Vector2(0f, 1f);
		tutText.rectTransform.anchoredPosition = new Vector2(5f, 0f);
		
		tutText.text = text;
		//tutorialPanel.gameObject.SetActive(true);
	}

	void HideTutorial()
	{
		tutorialPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 160f);
		tutorialPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 50f);
		tutText.text = null;
		tutText.rectTransform.position = new Vector3(0f, 0f, 0f);
		tutText.rectTransform.pivot = new Vector2(0f, 1f);
		tutText.rectTransform.anchoredPosition = new Vector2(5f, 0f);
		
		tutText.gameObject.SetActive(false);
		//tutorialPanel.gameObject.SetActive(false);
	}
}
