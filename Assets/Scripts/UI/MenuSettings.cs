﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MenuSettings : MonoBehaviour 
{

	public Toggle fullscreenToggle;
	public Dropdown resolutionDropdown;
	public Slider musicVolumeSlider;
	public Button applyButton;


	public Resolution[] resolutions;

	public GameSettings gameSettings;

	private void OnEnable()
	{
		gameSettings = new GameSettings();

		fullscreenToggle.onValueChanged.AddListener( delegate { OnFullscreenToggle(); });
		resolutionDropdown.onValueChanged.AddListener(delegate { OnResolutionChange(); });
		musicVolumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeChange(); });
		applyButton.onClick.AddListener(delegate { Apply(); });

		resolutions = Screen.resolutions;
		foreach (Resolution resolution in resolutions)
		{
			resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
		}
		LoadSettings();
	}

	public void OnFullscreenToggle()
	{
		gameSettings.isFullscreen = Screen.fullScreen = fullscreenToggle.isOn;
	}

	public void OnResolutionChange()
	{
		Screen.SetResolution(resolutions[resolutionDropdown.value].width, 
			resolutions[resolutionDropdown.value].height, Screen.fullScreen);
		gameSettings.resolutionIndex = resolutionDropdown.value;
	}

	public void OnMusicVolumeChange()
	{
		FMODUnity.RuntimeManager.GetBus("Bus:/").setVolume(musicVolumeSlider.value);
		gameSettings.musicVolume = musicVolumeSlider.value;
	} 


	public void SaveSettings()
	{
		string jsonData = JsonUtility.ToJson(gameSettings,true);
		File.WriteAllText(Application.persistentDataPath + "/gamesettings.json", jsonData);
	}

	public void LoadSettings()
	{
		if (File.Exists(Application.persistentDataPath + "/gamesettings.json"))
		{
			gameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(Application.persistentDataPath + "/gamesettings.json"));
			Screen.fullScreen = fullscreenToggle.isOn = gameSettings.isFullscreen;
			resolutionDropdown.value = gameSettings.resolutionIndex;
			musicVolumeSlider.value = gameSettings.musicVolume;

			resolutionDropdown.RefreshShownValue();
		}
		else
		{
			Screen.fullScreen = gameSettings.isFullscreen = false;
			Screen.SetResolution(1024, 768, Screen.fullScreen);
			for (int i = 0; i < resolutions.Length; i++)
			{
				if (resolutions[i].width == 1024 && resolutions[i].height == 768)
				{
					gameSettings.resolutionIndex = resolutionDropdown.value = i;
					break;
				}
			}
			gameSettings.musicVolume = musicVolumeSlider.value = 1f;
		}
		
	}

	public void Apply()
	{
		SaveSettings();
	}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
