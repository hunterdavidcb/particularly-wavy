﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarRevealer : MonoBehaviour 
{

	// Use this for initialization
	public int radius;

	private void Start()
	{
		FogOfWarManager.Instance.RegisterRevealer(this);
	}

	// Update is called once per frame
	void Update () 
	{
		
	}
}
