﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUse : MonoBehaviour 
{
	[SerializeField]
	float totalPowerUse = 0f;
	[SerializeField]
	float totalStaminaUse = 0f;
	public Image powerCharge;
	public Image staminaCharge;
	public Text powerText;
	public Text staminaText;
	public bool laserActive = false;

	float staminaScaler = 5f;
	float powerScaler = 10f;

	public float TotalPowerUse
	{
		get { return totalPowerUse; } private set { }
	}

	public float TotalStaminaUse
	{
		get { return totalStaminaUse; }
		private set { }
	}

	// Use this for initialization
	void Start () 
	{
		Laser[] lasers = FindObjectsOfType<Laser>();
		for (int i = 0; i < lasers.Length; i++)
		{
			lasers[i].LaserActivated += OnLaserActivated;
			lasers[i].LaserDeactivated += OnLaserDeactivated;
		}

		GrabMove[] gm = FindObjectsOfType<GrabMove>();
		for (int i = 0; i < gm.Length; i++)
		{
			gm[i].ObjectMoved += OnObjectMoved;
		}
		//staminaCharge.fillAmount = 0;
		staminaText.text = "0 Calories";
		powerText.text = "0 Watts";
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	protected void OnObjectMoved()
	{
		totalStaminaUse += Time.deltaTime;
		//if (totalStaminaUse > staminaScaler)
		//{
		//	staminaScaler += 5f;
		//}
		//staminaCharge.fillAmount = (staminaScaler-totalStaminaUse) / staminaScaler;
		staminaText.text = System.Math.Round((double)totalStaminaUse, 2).ToString() + " Calories";
	}

	protected void OnLaserActivated()
	{
		laserActive = true;
		StartCoroutine(IncrementPower());
	}



	protected void OnLaserDeactivated()
	{
		//Debug.Log("Received stop message");
		laserActive = false;
		//StopCoroutine(IncrementPower());
	}

	IEnumerator IncrementPower()
	{
		while (laserActive)
		{
			totalPowerUse += Time.deltaTime;
			//if (totalPowerUse > powerScaler)
			//{
			//	powerScaler += 10f;
			//}
			//powerCharge.fillAmount = (powerScaler - totalPowerUse) / powerScaler;
			powerText.text = System.Math.Round((double)totalPowerUse, 2).ToString() + " Watts";
			yield return null;
		}
	}

	private void OnDestroy()
	{
		StopCoroutine(IncrementPower());
	}

	private void OnDisable()
	{
		StopCoroutine(IncrementPower());
	}
	//TODO: recharge battery
}
