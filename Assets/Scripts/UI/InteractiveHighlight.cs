﻿using UnityEngine;

public class InteractiveHighlight : MonoBehaviour
{
	//public static string MATERIAL_OUTLINE_GREEN = "Materials/GreenOutline";
	//public static string MATERIAL_OUTLINE_YELLOW = "Materials/YellowOutline";
	//public static string MATERIAL_OUTLINE_RED = "Materials/RedOutline";

	public bool isInteractive = true;
	bool isHoveredOver = false;
	private Material hoverMaterial;
	private Material[] originalMaterials;
	private Material[] hoverMaterials;

	public enum HighlightColor { GREEN, YELLOW, RED };
	public HighlightColor highlightColor = HighlightColor.GREEN;


	void Awake()
	{
		//var color = GetHighlightColor(highlightColor);

		hoverMaterial = Resources.Load("Materials/Outline", typeof(Material)) as Material;

		originalMaterials = GetComponentInChildren<Renderer>().materials;
		hoverMaterials = new Material[originalMaterials.Length + 1];
		for (int i = 0; i < originalMaterials.Length; ++i)
		{
			hoverMaterials[i] = originalMaterials[i];
		}

		hoverMaterials[hoverMaterials.Length - 1] = hoverMaterial;
	}
	
	void OnMouseEnter()
	{
		if (isInteractive)
		{
			isHoveredOver = true;
			this.GetComponentInChildren<Renderer>().materials = hoverMaterials;
		}
	}

	void OnMouseExit()
	{
		if (isInteractive)
		{
			isHoveredOver = false;
			this.GetComponentInChildren<Renderer>().materials = originalMaterials;
		}
			
	}
	void OnMouseOver()
	{
		if (Input.GetMouseButtonDown(1) && isInteractive)
		{
			//Debug.Log("Right Click");
		}
	}

	//public static string GetHighlightColor(HighlightColor effectColor)
	//{
	//	//var color = "";
	//	//switch (effectColor)
	//	//{
	//	//	case HighlightColor.GREEN:
	//	//		color = MATERIAL_OUTLINE_GREEN;
	//	//		break;
	//	//	case HighlightColor.YELLOW:
	//	//		color = MATERIAL_OUTLINE_YELLOW;
	//	//		break;
	//	//	case HighlightColor.RED:
	//	//		color = MATERIAL_OUTLINE_RED;
	//	//		break;
	//	//	default:
	//	//		color = MATERIAL_OUTLINE_GREEN;
	//	//		break;
	//	//}
	//	//return color;
	//}

	public static void HighlightGameObject(GameObject go, HighlightColor effectColor)
	{
		//var color = GetHighlightColor(effectColor);
		var hoverMaterial = Resources.Load("Materials/Outline", typeof(Material)) as Material;
		//var hoverMaterial = Resources.Load(color, typeof(Material)) as Material;
		var originalMaterials = go.GetComponentInChildren<Renderer>().materials;
		var hoverMaterials = new Material[originalMaterials.Length + 1];
		for (int i = 0; i < originalMaterials.Length; ++i)
		{
			hoverMaterials[i] = originalMaterials[i];
		}
		hoverMaterials[hoverMaterials.Length - 1] = hoverMaterial;

		go.GetComponentInChildren<Renderer>().materials = hoverMaterials;
	}

	public static void RemoveHighlightGameObject(GameObject go)
	{        
		var hoverMaterials = go.GetComponentInChildren<Renderer>().materials;
		var originalMaterials = new Material[hoverMaterials.Length - 1];
		for (int i = 0; i < originalMaterials.Length; ++i)
		{
			hoverMaterials[i] = originalMaterials[i];
		}
		go.GetComponentInChildren<Renderer>().materials = originalMaterials;
	}
}
