﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class FogOfWarManager : MonoBehaviour 
{

	private int _textureSize = 256;
	public Color _fogOfWarColor;
	private LayerMask _fogOfWarLayer;

	private Texture2D _texture;
	private Color[] _pixels;
	private List<FogOfWarRevealer> _revealers;
	private int _pixelsPerUnit;
	private Vector2 _centerPixel;

	private static FogOfWarManager _instance;

	public static FogOfWarManager Instance
	{
		get { return _instance; }
	}

	private void Awake()
	{
		_fogOfWarLayer = LayerMask.GetMask("FogOfWar");

		_instance = this;
		var renderer = GetComponent<Renderer>();
		Material fogOfWarMat = null;

		if (renderer != null)
		{
			fogOfWarMat = renderer.material;
		}

		if (fogOfWarMat == null)
		{
			Debug.LogError("Material for Fog of War not found");
			return;
		}

		_texture = new Texture2D(_textureSize, _textureSize, TextureFormat.RGBA32, false);
		_texture.wrapMode = TextureWrapMode.Clamp;

		_pixels = _texture.GetPixels();
		ClearPixels();

		fogOfWarMat.mainTexture = _texture;

		_revealers = new List<FogOfWarRevealer>();

		_pixelsPerUnit = Mathf.RoundToInt(_textureSize / transform.lossyScale.x);

		_centerPixel = new Vector2(_textureSize * 0.5f, _textureSize * 0.5f);
	}

	private void ClearPixels()
	{
		for (var i = 0; i < _pixels.Length; i++)
		{
			_pixels[i] = _fogOfWarColor;
		}
	}

	public void RegisterRevealer(FogOfWarRevealer revealer)
	{
		_revealers.Add(revealer);
	}

	private void CreateCircle(int originX, int originY, int radius)
	{
		for (var y = -radius * _pixelsPerUnit; y <= radius * _pixelsPerUnit; ++y)
		{
			for (var x = -radius * _pixelsPerUnit; x <= radius * _pixelsPerUnit; ++x)
			{
				if (x * x + y * y <= (radius * _pixelsPerUnit) * (radius * _pixelsPerUnit))
				{
					//float d = (Vector2.Distance(new Vector2(originX,originY),new Vector2(x,y))/(float)(radius*_pixelsPerUnit));
					//float d = Mathf.Sqrt(x  * x  + y  * y )/radius;
					//d /= 255f;
					//Debug.Log(d);
					//Debug.Log("X: " + x + " Y: " + y);
					_pixels[(originY + y) * _textureSize + originX + x] = new Color(0f, 0f, 0f, 0f);
					//_pixels[(originY + y) * _textureSize + originX + x] = new Color(0f, 0f, 0f, d);
				}
			}
		}
	}

	//TODO Write line function to draw a line from startX, startY, to endX, endY with thickness R
	//public void Line(int x, int y, int x2, int y2)
	//{
	//	int w = x2 - x;
	//	int h = y2 - y;
	//	int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	//	if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	//	if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	//	if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	//	int longest = Mathf.Abs(w);
	//	int shortest = Mathf.Abs(h);
	//	if (!(longest > shortest))
	//	{
	//		longest = Mathf.Abs(h);
	//		shortest = Mathf.Abs(w);
	//		if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	//		dx2 = 0;
	//	}
	//	int numerator = longest >> 1;
	//	for (int i = 0; i <= longest; i++)
	//	{
	//		//putpixel(x, y, color);
	//		_pixels[(y+dy1)*_textureSize + x + dx1] = new Color(0f, 0f, 0f, 0f);
	//		numerator += shortest;
	//		if (!(numerator < longest))
	//		{
	//			numerator -= longest;
	//			x += dx1;
	//			y += dy1;
	//		}
	//		else
	//		{
	//			x += dx2;
	//			y += dy2;
	//		}
	//	}
	//}

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		ClearPixels();

		foreach (var revealer in _revealers)
		{
			// should do a raycast from the revealer to the camera.
			var screenPoint = Camera.main.WorldToScreenPoint(revealer.GetComponent<LineRenderer>().GetPosition(0));
			var ray = Camera.main.ScreenPointToRay(screenPoint);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 1000, _fogOfWarLayer,QueryTriggerInteraction.Collide))
			{
				// Translates the revealer to the center of the fog of war.
				// This way the position lines up with the center pixel and can be converted easier.
				var translatedPos = hit.point - transform.position;

				var pixelPosX = Mathf.RoundToInt(translatedPos.x * _pixelsPerUnit + _centerPixel.x);
				var pixelPosY = Mathf.RoundToInt(translatedPos.y * _pixelsPerUnit + _centerPixel.y);
				CreateCircle(pixelPosX, pixelPosY, revealer.radius);

				//var pixelPosStartX = Mathf.RoundToInt(translatedPos.x * _pixelsPerUnit + _centerPixel.x);
				//var pixelPosStartY = Mathf.RoundToInt(translatedPos.y * _pixelsPerUnit + _centerPixel.y);
				RaycastHit hit2;
				var screenPoint2 = Camera.main.WorldToScreenPoint(revealer.GetComponent<LineRenderer>().GetPosition(1));
				var ray2 = Camera.main.ScreenPointToRay(screenPoint2);
				if (Physics.Raycast(ray2,out hit2, 1000,_fogOfWarLayer,QueryTriggerInteraction.Collide))
				{
					var translatedPos2 = hit.point - transform.position;
					var pixelPosEndX = Mathf.RoundToInt(translatedPos2.x * _pixelsPerUnit + _centerPixel.x);
					var pixelPosEndY = Mathf.RoundToInt(translatedPos2.y * _pixelsPerUnit + _centerPixel.y);
					CreateCircle(pixelPosEndX, pixelPosEndY, revealer.radius);
				//	Line(pixelPosStartX, pixelPosStartY, pixelPosStartY, pixelPosEndY);
				}
				//var pixelPosEndX = Mathf.RoundToInt((translatedPos.x + )* _pixelsPerUnit + _centerPixel.x);

			}
		}

		_texture.SetPixels(_pixels);
		_texture.Apply(false);
	}
}
