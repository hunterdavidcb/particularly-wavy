﻿using DatabaseTut;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TargetAwareness : MonoBehaviour
{

	public delegate void TargetVictoryHandler();
	public event TargetVictoryHandler VictoryAchieved;
	public GameObject victoryPanel;
	GameObject[] targets;
	public Text statsText;
	Dictionary<GameObject, bool> hitTarget;
	Dictionary<GameObject, int> depths;
	bool victory = false;
	bool increment = true;
	[SerializeField]
	float levelTime = 0f;
	public Image hourHand;
	public Image minuteHand;

	[FMODUnity.EventRef]
	public string pressed;

	[FMODUnity.EventRef]
	public string triumph;

	float pu;
	float su;
	int elegance;
	// Use this for initialization
	void Start ()
	{
		targets = GameObject.FindGameObjectsWithTag("Target");
		hitTarget = new Dictionary<GameObject, bool>();
		depths = new Dictionary<GameObject, int>();
		for (int i = 0; i < targets.Length; i++)
		{
			targets[i].GetComponent<Target>().TargetReached += OnTargetReached;
			depths.Add(targets[i], -1);
			hitTarget.Add(targets[i], false);
		}

		if (Game.gameData== null)
		{
			Game.gameData = LoadAndSave.Load(Game.ProgressFile);
		}
		StartCoroutine(IncrementTime());
	}
	
	// Update is called once per frame
	void Update ()
	{
		//levelTime += Time.deltaTime;
		List<GameObject> temp = hitTarget.Keys.ToList();
		for (int i = 0; i < temp.Count; i++)
		{
			hitTarget[temp[i]] = false;
		}
	}

	IEnumerator IncrementTime()
	{
		while (increment)
		{
			levelTime += Time.deltaTime;
			minuteHand.transform.rotation = Quaternion.Euler(0f, 0f, -6f*levelTime );
			hourHand.transform.rotation = Quaternion.Euler(0f, 0f, - levelTime/10f);
			yield return null;
		}
	}

	protected void OnTargetReached(GameObject go, int maxDepth)
	{
		

		if (hitTarget.ContainsKey(go))
		{
			hitTarget[go] = true;
		}

		if (depths.ContainsKey(go))
		{
			depths[go] = maxDepth;
		}

		int counter = 0;
		int totalDepth = 0;
		for (int i = 0; i < targets.Length; i++)
		{
			if (hitTarget.ContainsKey(targets[i]))
			{
				if (hitTarget[targets[i]])
				{
					counter++;
				}
			}
			if (depths.ContainsKey(targets[i]))
			{
				if (depths[targets[i]] > totalDepth)
				{
					totalDepth = depths[targets[i]];
				}
				
			}
		}
		if (counter == targets.Length && !victory)
		{
			victory = true;
			OnVictoryAchieved();
			//Debug.Log("We hit all targets");
			//if (!messageSent)
			//{
			//OnTargetReached();
			//}
			increment = false;
			StopCoroutine(IncrementTime());

			FMODUnity.RuntimeManager.PlayOneShot(triumph);
			PowerUse powerUse = FindObjectOfType<PowerUse>();
			pu = powerUse.TotalPowerUse;
			su = powerUse.TotalStaminaUse;
			elegance = totalDepth;
			int nextLevelID = Game.levels.Get(SceneManager.GetActiveScene().name).ID + 1;
			PowerLevels example = new PowerLevels();
			example.LevelIndex = nextLevelID - 1;
			StaminaLevels sExample = new StaminaLevels();
			sExample.LevelIndex = nextLevelID - 1;
			EleganceLevels eExample = new EleganceLevels();
			eExample.LevelIndex = nextLevelID - 1;
			eExample.EleganceLevel = totalDepth;

			//TODO: display these numbers at the end of the level

			//Debug.Log(Game.levels.Get(SceneManager.GetActiveScene().name).ID + 1);
			if (!Game.gameData.unlockedLevels.Contains(nextLevelID) &&
				nextLevelID < SceneManager.sceneCountInBuildSettings)
			{
				//Debug.Log("Unlocking next level: " + (Game.levels.Get(SceneManager.GetActiveScene().name).ID + 1));
				Game.gameData.unlockedLevels.Add(nextLevelID);
				example.PowerLevel = pu;
				if (Game.gameData.powerLevels.Contains(example))
				{
					if (pu < Game.gameData.powerLevels[Game.gameData.powerLevels.IndexOf(example)].PowerLevel)
					{
						Debug.Log("Used less power, updating entry");
						Game.gameData.powerLevels[Game.gameData.powerLevels.IndexOf(example)].PowerLevel = pu;
					}
				}
				else
				{
					Game.gameData.powerLevels.Add(example);
				}
				
				sExample.StaminaLevel = su;

				if (Game.gameData.staminaLevels.Contains(sExample))
				{
					if (su < Game.gameData.staminaLevels[Game.gameData.staminaLevels.IndexOf(sExample)].StaminaLevel)
					{
						Debug.Log("Used less power, updating entry");
						Game.gameData.staminaLevels[Game.gameData.staminaLevels.IndexOf(sExample)].StaminaLevel = su;
					}
				}
				else
				{
					Game.gameData.staminaLevels.Add(sExample);
				}

				if (Game.gameData.eleganceLevels.Contains(eExample))
				{
					if (totalDepth < Game.gameData.eleganceLevels[Game.gameData.eleganceLevels.IndexOf(eExample)].EleganceLevel)
					{
						Debug.Log("Used less power, updating entry");
						Game.gameData.eleganceLevels[Game.gameData.eleganceLevels.IndexOf(eExample)].EleganceLevel = totalDepth;
					}
				}
				else
				{
					Game.gameData.eleganceLevels.Add(eExample);
				}

				if (levelTime < Game.gameData.levelTime)
				{
					Game.gameData.levelTime = levelTime;
				}

			}
			else if (nextLevelID < SceneManager.sceneCountInBuildSettings &&
				Game.gameData.unlockedLevels.Contains(nextLevelID) )
			{

				//Debug.Log("Unlocking next level: " + (Game.levels.Get(SceneManager.GetActiveScene().name).ID + 1));
				//Game.gameData.unlockedLevels.Add(nextLevelID);
				example.PowerLevel = pu;
				if (Game.gameData.powerLevels.Contains(example))
				{
					if (pu < Game.gameData.powerLevels[Game.gameData.powerLevels.IndexOf(example)].PowerLevel)
					{
						Debug.Log("Used less power, updating entry");
						Game.gameData.powerLevels[Game.gameData.powerLevels.IndexOf(example)].PowerLevel = pu;
					}
				}
				else
				{
					Game.gameData.powerLevels.Add(example);
				}

				sExample.StaminaLevel = su;

				if (Game.gameData.staminaLevels.Contains(sExample))
				{
					if (su < Game.gameData.staminaLevels[Game.gameData.staminaLevels.IndexOf(sExample)].StaminaLevel)
					{
						Debug.Log("Used less power, updating entry");
						Game.gameData.staminaLevels[Game.gameData.staminaLevels.IndexOf(sExample)].StaminaLevel = su;
					}
				}
				else
				{
					Game.gameData.staminaLevels.Add(sExample);
				}

				if (Game.gameData.eleganceLevels.Contains(eExample))
				{
					if (totalDepth < Game.gameData.eleganceLevels[Game.gameData.eleganceLevels.IndexOf(eExample)].EleganceLevel)
					{
						Debug.Log("Used less power, updating entry");
						Game.gameData.eleganceLevels[Game.gameData.eleganceLevels.IndexOf(eExample)].EleganceLevel = totalDepth;
					}
				}
				else
				{
					Game.gameData.eleganceLevels.Add(eExample);
				}

				if (levelTime < Game.gameData.levelTime)
				{
					Game.gameData.levelTime = levelTime;
				}

			}
			if (Game.gameData == null)
			{
				Debug.Log("It's null. WTF");
			}
			LoadAndSave.Save(Game.ProgressFile, Game.gameData);

			GrabMove[] gm = FindObjectsOfType<GrabMove>();
			for (int i = 0; i < gm.Length; i++)
			{
				Destroy(gm[i]);
			}

			

			StartCoroutine(Victory());
			
		}
	}

	IEnumerator Victory()
	{
		//yield return new WaitForSeconds();
		GetComponent<PowerUse>().laserActive = false;
		statsText.text = "Stamina Used " + su + "\n" 
			+ "Light Power Used " + pu + "\n"
			+ "Target Reached in " + elegance + " light rays \n"
			+ "Level Completed in " + levelTime + " seconds" ;
		victoryPanel.SetActive(true);
		while (victoryPanel.GetComponent<Image>().color.a < 0.8f)
		{
			victoryPanel.GetComponent<Image>().color = new Color(victoryPanel.GetComponent<Image>().color.r,
				victoryPanel.GetComponent<Image>().color.g, victoryPanel.GetComponent<Image>().color.b, 
				victoryPanel.GetComponent<Image>().color.a + Time.deltaTime*0.25f);
			yield return null;
		}
		victoryPanel.SetActive(true);
		Laser[] lasers = FindObjectsOfType<Laser>();
		for (int i = 0; i < lasers.Length; i++)
		{
			Destroy(lasers[i]);
		}

		LineRenderer[] lrs = FindObjectsOfType<LineRenderer>();
		for (int i = 0; i < lrs.Length; i++)
		{
			Destroy(lrs[i].gameObject);
		}

		RayNode[] raynodes = FindObjectsOfType<RayNode>();
		for (int i = 0; i < raynodes.Length; i++)
		{
			Destroy(raynodes[i].gameObject);
		}

	}

	public void OnContinuePressed()
	{
		//check if the next level exists
		FMODUnity.RuntimeManager.PlayOneShot(pressed);

		if (victory)
		{
			LevelData ld = Game.levels.Get(Game.levels.Get(SceneManager.GetActiveScene().name).ID + 1);
			//Debug.Log(ld.ID + " " + ld.Name);
			if (ld.ID != -1)
			{
				//Debug.Log("Loading");

				SceneManager.LoadSceneAsync(ld.ID);
				SceneManager.UnloadSceneAsync(SceneManager.GetSceneByBuildIndex(ld.ID - 1));

			}
		}
		else
		{

		}
		
	}

	public void OnReturnPressed()
	{
		//go back to main menu
		FMODUnity.RuntimeManager.PlayOneShot(pressed);
		//SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
		SceneManager.LoadSceneAsync(0);

	}

	protected void OnVictoryAchieved()
	{
		if (VictoryAchieved != null)
		{
			VictoryAchieved();
		}
	}
}
