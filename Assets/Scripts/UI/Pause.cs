﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour 
{

	public delegate void PauseHandler(bool b);
	public event PauseHandler PauseChanged;

	public GameObject pausePanel;
	public bool isPaused;
	// Use this for initialization
	void Start () 
	{
		Time.timeScale = 1.0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown("Cancel"))
		{

			switchPause();
		}
	}

	void PauseGame(bool state)
	{
		if (state)
		{
			pausePanel.SetActive(true);
			//Cursor.visible = true;
			Time.timeScale = 0.0f;
		}
		else
		{
			Time.timeScale = 1.0f;
			pausePanel.SetActive(false);
			//Cursor.visible = false;
		}
	}

	public void switchPause()
	{
		if (isPaused)
		{
			isPaused = false;
			PauseGame(false);
			//LoadAndSave.CleanScreenshots();
		}
		else
		{
			isPaused = true;
			PauseGame(true);
		}
		OnPauseChanged();
	}

	protected void OnPauseChanged()
	{
		if (PauseChanged != null)
		{
			PauseChanged(isPaused);
		}
	}

	public void ReturnToMenu()
	{
		SceneManager.LoadScene(0);
	}

	public void Quit()
	{
		Application.Quit();
	}
}
