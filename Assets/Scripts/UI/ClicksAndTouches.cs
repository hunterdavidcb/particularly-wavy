﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClicksAndTouches : MonoBehaviour 
{
	Camera camera;
	bool released = true;
	RaycastHit hit;
	GameObject selectedGameObject;
	// Use this for initialization
	void Start () 
	{
		if (camera == null)
		{
			camera = Camera.main;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = camera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				//Debug.Log(hit.transform.gameObject.name);
				selectedGameObject = hit.transform.gameObject;
				released = false;
			}
			else
			{
				//Debug.Log("Missed");
				selectedGameObject = null;
			}
		}
		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			Ray ray = camera.ScreenPointToRay(Input.GetTouch(0).position);
			if (Physics.Raycast(ray, out hit))
			{
				//Debug.Log(hit.transform.gameObject.name);
				selectedGameObject = hit.transform.gameObject;
				released = false;
			}
			else
			{
				//Debug.Log("Missed");
				selectedGameObject = null;
			}
		}
		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			//perform dragging
		}
		if (Input.touchCount == 2 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved))
		{
			//perform rotations
		}

		if (Input.GetMouseButtonUp(0))
		{
			released = true;
		}
		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			released = true;
		}
		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved && selectedGameObject != null && !released)
		{
			//send message to selectedGameObject
		}
		if (Input.GetMouseButton(0) && !released && selectedGameObject != null)
		{
			//send message to selectedGameObject
			//selectedGameObject.GetComponent<GrabMove>().Drag(mou);
		}
	}

}
