﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LinePolygonRenderer : MonoBehaviour
{
	public Material Material;

	public List<Vector3> points;
	public float Distance;

	void Start()
	{
		if (points == null)
		{
			Debug.Log("null list");
		}
		Vector2[] p = new Vector2[points.Count];
		for (int i = 0; i < points.Count; i++)
		{
			p[i] = new Vector2(points[i].x, points[i].y);
		}
		CreateLine(p);
		CalculateDistance();
	}

	void CreateLine(Vector2[] points)
	{
		//var _object = new GameObject("PolygonLine");
		var meshFilter = gameObject.GetComponent<MeshFilter>();
		var meshRenderer = gameObject.GetComponent<MeshRenderer>();

		meshFilter.sharedMesh = LinePolygon.CreateMesh(points, .125f);
		meshRenderer.material = this.Material;
	}

	void CalculateDistance()
	{
		Distance = 0f;
		for (int i = 0; i < points.Count-1; i++)
		{
			Distance += Vector3.Distance(points[i], points[i + 1]);
		}
	}

	public Vector3 GetClosestPoint(Vector3 point)
	{
		//Debug.Log(point);
		Vector3 start, end,startToPoint,startToEnd;

		List<Vector3> positions = new List<Vector3>();
		for (int i = 0; i < points.Count-1; i++)
		{
			start = points[i];
			end = points[i + 1];
			startToPoint = point - start;
			float dotProduct = Vector3.Dot(startToPoint, (end - start).normalized);
			Vector3 pos = start + (end - start).normalized * dotProduct;
			//start = points[i];
			//end = points[i+1];
			//startToPoint = point - start;
			//startToEnd = end - start;
			//float magSquared = startToEnd.sqrMagnitude;
			//float dotProduct = Vector3.Dot(startToPoint, startToEnd);
			//float normal = dotProduct / magSquared;
			//Vector3 pos = new Vector3(points[i].x + startToEnd.x * normal, points[i].y + startToEnd.y * normal, 0f);
			positions.Add(pos);
		}
		//Debug.Log(positions[positions.Count-1]);
		positions.Sort(new Vector3Comparer());
		return positions[0];
	}

	public static Vector3 GetClosestPoint(Vector3 point, Vector3 start, Vector3 end)
	{
		//Debug.Log(point);
		Vector3 startToPoint, startToEnd;
		startToEnd = end - start;
			startToPoint = point - start;
			float dotProduct = Vector3.Dot(startToPoint, startToEnd.normalized);
			Vector3 position = start + (end - start).normalized * dotProduct;
		//start = points[i];
		//end = points[i+1];
		//startToPoint = point - start;
		//startToEnd = end - start;
		//float magSquared = startToEnd.sqrMagnitude;
		//float dotProduct = Vector3.Dot(startToPoint, startToEnd);
		//float normal = dotProduct / magSquared;
		//Vector3 pos = new Vector3(points[i].x + startToEnd.x * normal, points[i].y + startToEnd.y * normal, 0f);
		return position;
	}
}

class Vector3Comparer : IComparer<Vector3>
{
	public int Compare(Vector3 a, Vector3 b)
	{
		if (a.y < b.y)
		{
			return -1;
		}

		if (Mathf.Approximately(a.y,b.y))
		{
			if (Mathf.Approximately(a.x,b.x))
			{
				return 0;
			}
			if (a.x < b.x)
			{
				return -1;
			}
		}
		return 1;
	}
}