﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splitter : MonoBehaviour 
{
	public Material[] targetMaterials;
	// Use this for initialization
	void Start () 
	{
		Color c = new Color();
		if (targetMaterials != null)
		{
			for (int i = 0; i < targetMaterials.Length; i++)
			{
				//set all hit by materials to false
				//hitByMaterial.Add(targetMaterials[i].color, false);
				c += targetMaterials[i].color;
			}
		}
		c.a = 0.3f;
		GetComponent<MeshRenderer>().material.color = c;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
