﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reflect : MonoBehaviour 
{

	public Material[] materialsToReflect;
	// Use this for initialization

	//use this to allow light to travel through and reflect at same time
	[Range(0,100)]
	public float opacity;
	


	void Start () 
	{
		//line = GetComponent<LineRenderer>();
		//line.numPositions = 2;
		//line.GetComponent<Renderer>().material = lineMaterial;
		//line.startWidth = .1f;
		Color c = new Color(0f,0f,0f);
		if (materialsToReflect != null)
		{
			for (int i = 0; i < materialsToReflect.Length; i++)
			{
				c += materialsToReflect[i].color;
			}
		}
		
		GetComponentInChildren<MeshRenderer>().material.color = c;
	}

	// Update is called once per frame
	void Update()
	{
		//if (line.numPositions ==2)
		//{
		//	CalculateReflections(line.numPositions - 1);
		//}
		
	}

	
}
