﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderTest : MonoBehaviour 
{
	LineRenderer lineRenderer;
	float offset;
	// Use this for initialization
	void Start () 
	{
		lineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		offset -= Time.deltaTime * 2f;
		if (offset < -.5f)
		{
			offset += .5f;
		}

		lineRenderer.materials[1].SetTextureOffset("_MainTex", new Vector2(offset / 4f, 0));
		lineRenderer.materials[2].SetTextureOffset("_MainTex", new Vector2(offset, 0));
		//lineRenderer.material.SetTextureOffset("_Tex", new Vector2(offset, 0));
	}
}
