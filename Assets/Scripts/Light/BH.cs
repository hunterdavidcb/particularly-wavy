﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BH : MonoBehaviour 
{
	float blackHoleDistance = 2f;

	RayNode root;
	public Transform parent;

	Vector3 laserOrigin;
	Vector3 laserDirection;
	float range = 20.0f;
	AnimationCurve curve;
	int numberOfPositions = 40;

	List<Collider> colliders = new List<Collider>();

	float speed = 4f;
	float blackHoleMass = 10;

	// Use this for initialization
	void Start () 
	{
		curve = new AnimationCurve(new Keyframe(0, 0.15625f), new Keyframe(1, 0.15625f));
		laserOrigin = transform.position;
		parent = transform.parent;
		laserDirection = (transform.position - parent.position).normalized;
		root = gameObject.GetComponent<RayNode>();
		root.depth = 1;
		root.rayLR = GetComponent<LineRenderer>();
		root.rayLR.positionCount = 40;
		root.rayLR.widthCurve = curve;
		root.rayLR.SetPosition(0, transform.position);
		for (int i = 1; i < root.rayLR.positionCount; i++)
		{
			root.rayLR.SetPosition(i, transform.position + laserDirection * range * (float)i/(float)root.rayLR.positionCount);
		}

	}


	
	// Update is called once per frame
	void Update () 
	{
		CheckBlackHoles(root);
	}

	private Vector3[] SetPositions(Vector3 start, Vector3 end, int steps)
	{
		Vector3[] v = new Vector3[steps];
		v[0] = start;
		v[steps - 1] = end;
		Vector3 delta = (end - start).normalized * Vector3.Distance(start, end) / (float)steps;
		for (int i = 1; i < steps - 1; i++)
		{
			v[i] = start + delta * (float)i;
		}
		return v;
	}

	private Vector3[] UpdateLRPositions(GameObject go, RayNode node, Vector3 pos, Vector3 center,Vector3 vel,float rad)
	{
		Vector3[] lerpPositions = new Vector3[numberOfPositions];
		List<Vector3> tempList = new List<Vector3>();
		lerpPositions[0] = node.rayLR.GetPosition(0);
		lerpPositions[1] = pos;
		bool hitHole = false;
		Vector3 gravityVec;
		float d;
		//Debug.Log(Vector3.Angle(center-lerpPos, center-nSave));
		Vector3 accel = Vector3.zero;
		Vector3 previous = pos;
		float time = 0.01f;
		float diff = 0.1f;
		for (int i = 0; i < 150; i++)
		{

			gravityVec = center - previous;
			d = Vector3.Distance(center, previous);
			accel.x = blackHoleMass / (d * d * d * d * d);
			accel.y = blackHoleMass / (d * d * d * d * d);
			accel = Vector3.Scale(accel, gravityVec);
			vel += accel * time;
			previous += vel * time;

			if (i % 4 == 0)
			{
				tempList.Add(previous);
			}
			Debug.DrawLine(center, previous, Color.red, 1f);
			if (Vector3.Distance(previous, center) < rad+diff)
			{
				tempList.Add(previous);
				node.hitObject = go;
				hitHole = true;
				break;
			}
			else if (Vector3.Distance(previous, center) > blackHoleDistance + rad)
			{
				tempList.Add(previous);
				break;
			}
		}



		//node.rayLR.SetPosition(1, lerpPos);
		float dist = Vector3.Distance(pos, node.rayLR.GetPosition(0));
		for (int i = 0; i < tempList.Count; i++)
		{
			//Debug.Log(lerpPositions.Count);
			if (!hitHole)
			{
				if (i + 2 < lerpPositions.Length && i + 2 < node.rayLR.positionCount) //&&i % 2 == 0 )
				{
					lerpPositions[i + 2] = tempList[i];
					//node.rayLR.SetPosition(2 + i, lerpPositions[i]);
					dist += Vector3.Distance(lerpPositions[i + 2], lerpPositions[i + 1]);
				}

				if (i + 2 == tempList.Count && i + 2 < node.rayLR.positionCount && i + 2 < lerpPositions.Length)
				{
					float remainingDist = range - dist;
					//Debug.Log(remainingDist);
					//Debug.Log(i);
					int remainingPos = node.rayLR.positionCount - (i + 2);
					float deltaPos = remainingDist / (float)remainingPos;
					Vector3 dir = (lerpPositions[i + 2] - lerpPositions[i + 1]).normalized;
					//Debug.Log("remaining positions " + remainingPos);
					//Debug.Log("delta position " + deltaPos);
					//Debug.Log(dir);
					for (int j = 1; j < remainingPos; j++)
					{
						lerpPositions[2 + i + j] = lerpPositions[i] + dir * deltaPos * j;
						//node.rayLR.SetPosition(2 + i + j, lerpPositions[i] + dir * deltaPos * j);
						//Debug.Log(lerpPositions[i] + dir * deltaPos * j);
					}
					break; //probably not needed
				}
			}
			else
			{
				if (i + 2 < lerpPositions.Length && i + 2 < node.rayLR.positionCount) //&&i % 2 == 0 )
				{
					if (Vector3.Distance(tempList[i], center) > rad+diff)
					{
						lerpPositions[i + 2] = tempList[i];
						//node.rayLR.SetPosition(2 + i, lerpPositions[i]);
						dist += Vector3.Distance(lerpPositions[i + 2], lerpPositions[i + 1]);
					}
					else
					{
						lerpPositions[i + 2] = center;
						int remainingPos = node.rayLR.positionCount - (i + 3);
						//lerpPositions[i + 1] = center;
						lerpPositions[i + 3] = center;
						//Debug.Log(remainingPos);
						for (int j = 1; j < remainingPos; j++)
						{
							lerpPositions[3+i + j] = center;
							//Debug.Log(3+i + j + " " + center);
							//node.rayLR.SetPosition(2 + i + j, lerpPositions[i] + dir * deltaPos * j);
							//Debug.Log(lerpPositions[i] + dir * deltaPos * j);
						}
						break;
					}
				}
			}
			
		}
		return lerpPositions;
	}

	private void CheckBlackHoles(RayNode node)
	{
		Vector3 norm = (node.rayLR.GetPosition(1) - node.rayLR.GetPosition(0)).normalized;
		
		Vector3 lerpPos = new Vector3();
		Vector3 t = Vector3.positiveInfinity;
		Vector3 center = new Vector3();
		int index = -1;
		float rad = 0;
		
		RaycastHit hit = new RaycastHit();
		bool influenced = false;
		for (int i = 0; i < node.rayLR.positionCount; i++)
		{
			Ray ray;
			
			if (i+i < node.rayLR.positionCount)
			{
				ray = new Ray(node.rayLR.GetPosition(i), node.rayLR.GetPosition(i + 1) - node.rayLR.GetPosition(i));
				float maxRange = Vector3.Distance(node.rayLR.GetPosition(i), node.rayLR.GetPosition(i + 1));
				if (Physics.SphereCast(ray, blackHoleDistance, out hit, maxRange, LayerMask.GetMask("BlackHole")))
				{
					influenced = true;
					if (!colliders.Contains(hit.collider))
					{
						index = i;
						Debug.DrawLine(hit.point, node.rayLR.GetPosition(index), Color.blue);
						//Debug.Log("hit and raypos" + Vector3.Distance(hit.point, node.rayLR.GetPosition(index)));
						lerpPos = hit.point + hit.normal * blackHoleDistance;// + hit.normal * hit.collider.gameObject.GetComponent<SphereCollider>().radius;
						rad = hit.transform.GetComponent<SphereCollider>().radius;
						center = hit.transform.position;
						if (Vector3.Distance(center,lerpPos) < Vector3.Distance(center,t))
						{
							t = lerpPos;
						}
						//lerpPositions.Add(lerpPos);
						Debug.DrawLine(hit.point, hit.point + hit.normal * blackHoleDistance, Color.green);
						colliders.Add(hit.collider);
						//Debug.Log("hit and shortest" + Vector3.Distance(hit.point, hit.point + hit.normal * blackHoleDistance));
						//Debug.Log(hit.point + hit.normal * blackHoleDistance); //this gives the point on the light beam from which we intersected the black hole
						//Debug.Log("hit at " + hit.point + " index is " + i);
						Vector3 velocity = (node.rayLR.GetPosition(i + 1) - node.rayLR.GetPosition(i)).normalized*speed;
						node.rayLR.SetPositions(UpdateLRPositions(hit.collider.gameObject,node,lerpPos, center,velocity,rad));
						break;
					}
					else
					{
						index = i;
						Debug.DrawLine(hit.point, node.rayLR.GetPosition(index), Color.blue);
						//Debug.Log("hit and raypos" + Vector3.Distance(hit.point, node.rayLR.GetPosition(index)));
						lerpPos = hit.point + hit.normal * blackHoleDistance;// + hit.normal * hit.collider.gameObject.GetComponent<SphereCollider>().radius;
						rad = hit.transform.GetComponent<SphereCollider>().radius;
						center = hit.transform.position;
						if (Vector3.Distance(center, lerpPos) < Vector3.Distance(center, t))
						{
							t = lerpPos;
						}
						//lerpPositions.Add(lerpPos);
						Debug.DrawLine(hit.point, hit.point + hit.normal * blackHoleDistance, Color.green);
						Vector3 velocity = (node.rayLR.GetPosition(i + 1) - node.rayLR.GetPosition(i)).normalized * speed;
						node.rayLR.SetPositions(UpdateLRPositions(hit.collider.gameObject, node, lerpPos, center, velocity, rad));
						break;
					}
					
				}
			}
		}

		if (!influenced)
		{
			node.rayLR.SetPositions(SetPositions(laserOrigin, laserOrigin + laserDirection * range, numberOfPositions));
		}

		foreach (var child in node.children)
		{
			CheckBlackHoles(child);
		}
	}
}
