﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatEffect : MonoBehaviour 
{
	Vector4 color;
	float maxEffectTime = 10f;


	float blackHoleDistance = 2f;
	float speed = 4f;
	float blackHoleMass = 10;


	float timer = 10f;
	float effectTime;
	float strength;
	[SerializeField]
	float temp;
	public Material[] refractionMaterials;
	public GameObject metaballSelf;
	public GameObject metaballLight;

	[SerializeField]
	Vector3[] verts;

	[SerializeField]
	int index = 0;
	bool previouslyActive = false;

	RayNode root;

	Vector3 laserOrigin;
	Vector3 laserDirection;
	Vector3 normal;
	float range = 20.0f;
	public Material WhiteLight;


	int layermask;
	int prismRaycastLayerMask;
	int lensRaycastLayerMask;
	int filterRaycastLayerMask;
	int burnableLayer;


	AnimationCurve curve;
	bool laserOn;
	Transform parent;




	// Use this for initialization
	void Start () 
	{
		curve = new AnimationCurve(new Keyframe(0, 0.15625f), new Keyframe(1, 0.15625f));
		strength = effectTime = temp = 0f;
		verts = GetComponent<MeshFilter>().mesh.vertices;
		root = GetComponent<RayNode>();
		root.rayLR = GetComponent<LineRenderer>();
		root.depth = 1;
		root.rayLR.positionCount = 2;
		//root.rayLR.material = refractionMaterials[0];
		root.rayLR.widthCurve = curve;
		root.rayLR.enabled = false;
		root.metaball = metaballLight;
		laserOrigin = transform.position;
		parent = transform.parent;
		laserDirection = (transform.position - parent.position).normalized;
		root.rayLR.SetPosition(0, laserOrigin);
		root.rayLR.SetPosition(1, laserOrigin);

		layermask = LayerMask.GetMask("Prism") | LayerMask.GetMask("Default") | LayerMask.GetMask("MoveableObjects") |
			LayerMask.GetMask("BlackHole") | LayerMask.GetMask("Lens") | LayerMask.GetMask("Burnable") | LayerMask.GetMask("Filter");
		prismRaycastLayerMask = LayerMask.GetMask("Prism");
		lensRaycastLayerMask = LayerMask.GetMask("Lens");
		burnableLayer = LayerMask.GetMask("Burnable");
		filterRaycastLayerMask = LayerMask.GetMask("Filter");

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (effectTime > 0.5f && effectTime <1f)
		{
			strength -= Time.deltaTime;
			effectTime -= Time.deltaTime;
			GetComponent<MeshRenderer>().material.SetFloat("_MaxDistance", strength);
			GetComponent<MeshRenderer>().material.SetFloat("_EffectTime", effectTime);
			//GetComponent<MeshRenderer>().material.SetVector("_Color", Color.white);
		}
		else if (effectTime > 0.5f)
		{
			effectTime -= Time.deltaTime;
		}
		else
		{
			strength = 0;
			effectTime = 0;
			GetComponent<MeshRenderer>().material.SetFloat("_MaxDistance", strength);
			GetComponent<MeshRenderer>().material.SetFloat("_EffectTime", effectTime);
			GetComponent<MeshRenderer>().material.SetVector("_Color", Color.white);
		}

		if (timer > 0f && previouslyActive)
		{
			metaballSelf.SetActive(true);
			root.rayLR.enabled = true;
			root.metaball.SetActive(true);
			root.rayLR.SetPosition(0, laserOrigin);
			Emit(laserOrigin, laserDirection, layermask, root);

		}
		else
		{
			metaballSelf.SetActive(false);
			root.rayLR.enabled = false;
			root.metaball.SetActive(false);
		}
		
	}

	public void Heat(Vector3 start, RaycastHit hit)
	{
		//Debug.Log("I'm hit");
		//laserOrigin = transform.position;
		laserOrigin= new Vector3(hit.point.x, hit.point.y, 0f);
		normal = laserDirection = hit.normal;

		laserDirection += new Vector3(Random.Range(-.12f, .125f), Random.Range(-.125f, .125f), 0f);

		Vector3 L = (hit.point - start).normalized;
		Vector3 newPos;
		newPos = L - 2 * (Vector3.Dot(L, laserDirection)) * laserDirection;
		newPos *= range;
		newPos.z = 0;
		//laserDirection = (te - transform.position).normalized;
		Debug.DrawLine(laserOrigin,laserOrigin + newPos);
		effectTime = 1f;
		strength += Time.deltaTime;
		//Debug.Log(strength);
		GetComponent<MeshRenderer>().material.SetVector("_Position", transform.InverseTransformPoint(hit.point));
		GetComponent<MeshRenderer>().material.SetFloat("_MaxDistance",strength);
		GetComponent<MeshRenderer>().material.SetFloat("_EffectTime", effectTime);
		GetComponent<MeshRenderer>().material.SetVector("_Color", Color.red);
		if (!previouslyActive)
		{
			previouslyActive = true;
			StartCoroutine(Timer());
		}
	}

	private IEnumerator Timer()
	{
		Debug.Log("starting timer");
		while (timer > 0)
		{
			timer -= Time.deltaTime;
			laserDirection = normal + new Vector3(Random.Range(-.12f, .125f), Random.Range(-.125f, .125f), 0f);
			yield return null;
		}

		Debug.Log("Stopping timer");
		previouslyActive = false;
		timer = 10f;
		StopAllCoroutines();

	}

	protected void OnObjectMoved()
	{
		if (laserOn)
		{
			Emit(laserOrigin, laserDirection, layermask, root);
		}

	}

	private void NoBlackHole(RaycastHit rHit, RayNode node)
	{
		Reflect hitReflect = rHit.collider.GetComponent<Reflect>();
		Vector3 h = new Vector3(rHit.point.x, rHit.point.y, 0f);
		if (hitReflect != null)
		{
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			//Reflect(node, 0, node.rayLR.GetPosition(0), 1, rHit);
			//StopAllCoroutines();
		}
		else if (rHit.collider.gameObject.name == "prism")
		{
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}

			//Refract(node, node.rayLR.GetPosition(0), rHit);
			//StopAllCoroutines();
			//Debug.Log("Hit prism");
		}
		else if (rHit.collider.gameObject.GetComponent<Target>() != null)
		{

			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);

			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			node.hitObject = rHit.collider.gameObject;
			//OnTargetHit(rHit.collider.gameObject, node);
			//StopAllCoroutines();
			node.PruneWholeTree();
		}
		else if (rHit.collider.gameObject.GetComponent<Filter>() != null)
		{

			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			//StopAllCoroutines();
			//HitFilter(node, rHit);
		}
		else if (rHit.collider.gameObject.GetComponent<ChargeObject>() != null)
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			node.hitObject = rHit.collider.gameObject;
			node.PruneWholeTree();
			//OnHitCharge(rHit.collider.gameObject, node.rayLR.material);
		}
		else if (rHit.collider.gameObject.GetComponent<Splitter>() != null)
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			//HitSplitter(node, rHit);
		}
		else if (rHit.collider.gameObject.tag == "lens")
		{
			//StopAllCoroutines();
			//Debug.Log("Hit lens");
			//Debug.Log(rHit.point);
			//Debug.Log(rHit.normal);
			//Debug.Log("hit lens " + h);
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//HitLens(node, rHit);
		}
		else if (rHit.collider.tag == "Breakable" && node.intensified)
		{
			//HitBreakable(node, rHit);
		}
		else
		{
			//we hit something that does not relfect light
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			//StopAllCoroutines();
			node.hitObject = rHit.collider.gameObject;
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			node.PruneWholeTree();
		}
	}

	private void BlackHole(RaycastHit rHit, RayNode node, LayerMask mask)
	{
		//if a black hole is encountered, check if it is contained in the colliderList
		// (collider list should use RayNode as key, then store the black hole)
		//if it is, simply update the children of this node,
		//if it is not, add it and create the child of this node.
		//the next node must be set as the child of the first child
		//the final child must be marked as uninfluenced by the black hole,
		//but the others must be marked as influenced. this will be used to prevent
		// them from recurvely calling checkblackholes
		// call emit for each child
		Vector3 lerpPos = new Vector3();
		Vector3 center = new Vector3();
		float rad = 0;
		//the sphere cast above ignores all colliders except those on the black hole layer
		//now that we know it is possible that this ray of light is influenced by a black hole,
		//this raycast is to check that there is not some other collider inbetween
		RaycastHit doubleCheckHit;
		lerpPos = rHit.point + rHit.normal * blackHoleDistance;
		if (Physics.Raycast(lerpPos, rHit.point - lerpPos, out doubleCheckHit, range, mask))
		{
			if (rHit.collider == doubleCheckHit.collider)
			{
				//Debug.DrawLine(rHit.point, node.rayLR.GetPosition(0), Color.blue);
				rad = rHit.transform.GetComponent<SphereCollider>().radius;
				center = rHit.transform.position;
				//Debug.DrawLine(rHit.point, lerpPos, Color.green);
				node.rayLR.SetPosition(1, lerpPos);
				Vector3 velocity = (node.rayLR.GetPosition(1) - node.rayLR.GetPosition(0)).normalized * speed;
				node.hitObject = null;
				//SpawnBlackHoleNodes(rHit.collider.gameObject, node, lerpPos, center, velocity, rad);
			}
		}
	}

	private void Emit(Vector3 start, Vector3 direction, LayerMask mask, RayNode node)
	{
		RaycastHit pHit;


		RaycastHit hit = new RaycastHit();


		Ray ray;

		//Debug.Log("Emit");
		ray = new Ray(start, direction);
		bool rEmit = Physics.Raycast(ray, out pHit, range, mask, QueryTriggerInteraction.Ignore);
		bool bEmit = Physics.SphereCast(ray, blackHoleDistance, out hit, range, LayerMask.GetMask("BlackHole"));
		if (rEmit && bEmit)
		{
			if (Vector3.Distance(pHit.point, start) < Vector3.Distance(hit.point, start))
			{
				//node.hitObject = null;
				//Debug.Log("No black hole version");
				NoBlackHole(pHit, node);
			}
			else
			{
				//node.hitObject = null;
				Debug.Log("Black hole version");
				BlackHole(hit, node, mask);
			}

		}
		else if (rEmit && !bEmit)
		{
			NoBlackHole(pHit, node);
		}
		else if (!rEmit && bEmit)
		{
			BlackHole(hit, node, mask);
		}
		else
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(1, node.gameObject.transform.position + direction * range);
			node.hitObject = null;
			node.PruneWholeTree();
		}
		node.HandleMetaBall();
	}

	private bool BHEmit(RayNode node, Vector3 startPosition, Vector3 direction)
	{
		RaycastHit rHit;
		Vector3 h;
		bool hitSomtheing = false;
		float dist = (node.rayLR.GetPosition(1) - node.rayLR.GetPosition(0)).magnitude;
		if (Physics.Raycast(startPosition, direction, out rHit, dist))
		{
			Reflect hitReflect = rHit.collider.GetComponent<Reflect>();
			h = new Vector3(rHit.point.x, rHit.point.y, 0f);
			hitSomtheing = true;
			if (hitReflect != null)
			{
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				//Reflect(node, 0, node.rayLR.GetPosition(0), 1, rHit);

			}
			else if (rHit.collider.gameObject.name == "prism")
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}

				//Refract(node, node.rayLR.GetPosition(0), rHit);
				//Debug.Log("Hit prism");
			}
			else if (rHit.collider.gameObject.GetComponent<Target>() != null)
			{

				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);

				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				node.hitObject = rHit.collider.gameObject;
				//OnTargetHit(rHit.collider.gameObject, node);
				node.PruneWholeTree();
			}
			else if (rHit.collider.gameObject.GetComponent<Filter>() != null)
			{

				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				//HitFilter(node, rHit);
			}
			else if (rHit.collider.gameObject.GetComponent<ChargeObject>() != null)
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				node.hitObject = rHit.collider.gameObject;
				node.PruneWholeTree();
				//OnHitCharge(rHit.collider.gameObject, node.rayLR.material);
			}
			else if (rHit.collider.gameObject.GetComponent<Splitter>() != null)
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				//HitSplitter(node, rHit);
			}
			else if (rHit.collider.gameObject.tag == "lens")
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//HitLens(node, rHit);
			}
			else if (rHit.collider.tag == "Breakable" && node.intensified)
			{
				//HitBreakable(node, rHit);
			}
			else
			{
				//we hit something that does not relfect light
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				Debug.Log(node.gameObject.name + " has hit " + rHit.collider.name);
				node.hitObject = rHit.collider.gameObject;
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				node.PruneWholeTree();
			}

		}
		return hitSomtheing;
		//we do not change the ending position in blackhole emit because we want it to stay the same
		//as set by the blackhole calculations

	}
}
