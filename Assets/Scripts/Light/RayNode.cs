﻿using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(CapsuleCollider))]
public class RayNode : MonoBehaviour
{
	public GameObject hitObject;
	public List<RayNode> children;
	public ParticleSystem particleSystem;
	public LineRenderer rayLR;
	public int depth;
	public bool influencedByBlackHole;
	public bool intensified;
	public GameObject metaball;

	float offset;

	private void Start()
	{
	}

	private void Update()
	{
		offset -= Time.deltaTime * 2f;
		if (offset < -.5f)
		{
			offset += .5f;
		}
		rayLR.materials[1].SetTextureOffset("_MainTex", new Vector2(offset / 4f, 0));
		rayLR.materials[2].SetTextureOffset("_MainTex", new Vector2(offset, 0));

	}


	public RayNode()
	{
		children = new List<RayNode>();
	}

	public RayNode(GameObject RO)
	{
		hitObject = RO;
		children = new List<RayNode>();
	}

	public void PruneSubTree(int childIndex)
	{
		//validate index
		if (childIndex > -1 && childIndex < children.Count)
		{
			RayNode toPrune = children[childIndex];
			//set the hitObject to null
			toPrune.hitObject = null;

			List<RayNode> ch = new List<RayNode>();
			for (int i = 0; i < toPrune.children.Count; i++)
			{
				ch.Add(toPrune.children[i]);
			}

			//reverse order is important in order to prevent skipping errors
			for (int i = ch.Count - 1; i > -1; i--)
			{
				toPrune.PruneSubTree(i);
			}
			Destroy(toPrune.gameObject);
			children.Remove(toPrune);
		}
	}

	public List<RayNode> GetChildren()
	{
		return children;
	}

	public void PruneWholeTree()
	{
		if (children.Count > 0)
		{
			List<RayNode> ch = new List<RayNode>();
			for (int i = 0; i < children.Count; i++)
			{
				ch.Add(children[i]);
			}

			//reverse order is important in order to prevent skipping errors
			for (int i = ch.Count-1; i > -1; i--)
			{
				PruneSubTree(i);
			}
		}
	}

	public void UpdateChildren(Vector3 startPosition)
	{
		for (int i = 0; i < children.Count; i++)
		{
			children[i].gameObject.transform.position = startPosition;
			children[i].rayLR.SetPosition(0, startPosition);
		}
	}

	public void HandleMetaBall()
	{
		metaball.transform.position = (rayLR.GetPosition(0) + rayLR.GetPosition(1)) / 2f;
		Vector3 dir = rayLR.GetPosition(1) - rayLR.GetPosition(0);
		metaball.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg, Vector3.forward);
		float scaleFactor = 0;
		
		//set scaling factor in case of low distances
		if (Vector3.Distance(rayLR.GetPosition(1), rayLR.GetPosition(0)) < 5f)
		{
			scaleFactor = 0.2f;
		}

		metaball.transform.localScale = new Vector3((scaleFactor + 1.1f) * Vector3.Distance(rayLR.GetPosition(1), rayLR.GetPosition(0)), 2f, 1f);
	}
}
