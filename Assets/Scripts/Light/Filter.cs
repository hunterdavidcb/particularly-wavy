﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Filter : MonoBehaviour 
{
	public Material[] materialsToPass;
	// Use this for initialization
	void Start () 
	{
		Color c = new Color(0f, 0f, 0f);
		if (materialsToPass != null)
		{
			for (int i = 0; i < materialsToPass.Length; i++)
			{
				c += materialsToPass[i].color;
			}
		}

		GetComponent<MeshRenderer>().material.color = c;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
