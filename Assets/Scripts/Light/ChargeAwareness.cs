﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeAwareness : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		ChargeObject[] co = FindObjectsOfType<ChargeObject>();
		for (int i = 0; i < co.Length; i++)
		{
			co[i].Charged += OnCharged;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	protected void OnCharged(GameObject go)
	{
		Debug.Log(go.name + " is now charged.");
	}
}
