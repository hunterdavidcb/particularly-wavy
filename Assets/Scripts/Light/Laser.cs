﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]

public class Laser : MonoBehaviour
{

	public delegate void LaserEventHandler();
	public event LaserEventHandler LaserActivated;
	public event LaserEventHandler LaserDeactivated;

	float blackHoleDistance = 2f;
	float speed = 4f;
	float blackHoleMass = 10;


	RayNode root;

	Vector3 laserOrigin;
	Vector3 laserDirection;
	float range = 20f;
	public Material lineMaterial;
	public Material[] refractionMaterials;
	public Transform parent;

	public bool RequiresPowerSource;

	public Material WhiteLight;
	int layermask;
	int prismRaycastLayerMask;
	int lensRaycastLayerMask;
	int filterRaycastLayerMask;
	int burnableLayer;
	bool laserOn;
	bool hasPowerSource;
	GameObject Connection;
	public GameObject metaball;
	AnimationCurve curve;
	Vector3 touchPos;

	[FMODUnity.EventRef]
	public string LaserOn;
	[FMODUnity.EventRef]
	public string LaserOff;
	[FMODUnity.EventRef]
	public string Ping;

	float timerMax = 0.05f;
	float timer;

	public bool IsLaserOn
	{
		get {return laserOn; }
		private set {; }
	}

	private void OnDestroy()
	{
		if (LaserDeactivated != null)
		{
			LaserDeactivated();
		}

	}

	private void Awake()
	{
		Connection = transform.parent.Find("Connector").gameObject;
		Connection.GetComponent<Connection>().parent = this.gameObject;
	}

	private void OnEnable()
	{
		Laser[] lasers = FindObjectsOfType<Laser>();
		for (int i = 0; i < lasers.Length; i++)
		{
			if (lasers[i].IsLaserOn)
			{



				//subscribe to GrabMove delegates
				
				laserOn = true;
				//OnLaserActivated();
				//metaball.SetActive(true);
				//Emit(laserOrigin, laserDirection, layermask, root);
				break;

			}
		}

		GrabMove[] gm = FindObjectsOfType<GrabMove>();
		for (int j = 0; j < gm.Length; j++)
		{
			gm[j].ObjectMoved += OnObjectMoved;
		}

		Target[] targets = FindObjectsOfType<Target>();
		for (int i = 0; i < targets.Length; i++)
		{
			Debug.Log("updating " + targets[i].name + " with " + this.name);
			targets[i].UpdateLasers(this);
		}
	}


	void Start()
	{
		
		timer = timerMax;
		curve = new AnimationCurve(new Keyframe(0, 0.15625f), new Keyframe(1, 0.15625f));
		laserOrigin = transform.position;
		parent = transform.parent;
		laserDirection = (transform.position - parent.position).normalized;


		layermask = LayerMask.GetMask("Prism") | LayerMask.GetMask("Default") | LayerMask.GetMask("MoveableObjects") |
			LayerMask.GetMask("BlackHole") | LayerMask.GetMask("Lens") | LayerMask.GetMask("Burnable") | LayerMask.GetMask("Filter");
		prismRaycastLayerMask = LayerMask.GetMask("Prism");
		lensRaycastLayerMask = LayerMask.GetMask("Lens");
		burnableLayer = LayerMask.GetMask("Burnable");
		filterRaycastLayerMask = LayerMask.GetMask("Filter");


		//set up RayNode variables
		root = gameObject.GetComponent<RayNode>();
		root.depth = 1;
		root.rayLR = GetComponent<LineRenderer>();
		root.rayLR.positionCount = 2;
		root.rayLR.material = lineMaterial;
		root.rayLR.widthCurve = curve;
		root.metaball = metaball;
		metaball.SetActive(false);

		root.rayLR.SetPosition(0, transform.position);
		root.rayLR.SetPosition(1, transform.position);
		root.rayLR.enabled = false;


		////subscribe to GrabMove delegates
		//GrabMove[] gm = FindObjectsOfType<GrabMove>();
		//for (int i = 0; i < gm.Length; i++)
		//{
		//	gm[i].ObjectMoved += OnObjectMoved;
		//}
	}


	void Update()
	{

		laserDirection = (transform.position - parent.position).normalized;
		laserOrigin = transform.position;
		HandleInput();
	}

	void HandleInput()
	{
		if (Input.GetKeyDown(KeyCode.Space) && !laserOn)
		{
			laserOn = true;
			root.rayLR.enabled = true;
			OnLaserActivated();
			Emit(laserOrigin, laserDirection, layermask, root);
		}
		else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			RaycastHit hit;
			if (Physics.Raycast(Camera.main.transform.position, touchPos, out hit))
			{
				if (hit.collider.tag == "Laser")
				{
					if (!laserOn)
					{
						laserOn = true;
						root.rayLR.enabled = true;
						OnLaserActivated();
						Emit(laserOrigin, laserDirection, layermask, root);
					}
					else
					{
						laserOn = false;
						root.rayLR.enabled = false;
						OnLaserDeactivated();
					}
				}
			}
		}
		else if (Input.GetKeyDown(KeyCode.Space) && laserOn)
		{
			laserOn = false;
			root.rayLR.enabled = false;
			OnLaserDeactivated();
		}
	}

	/// <summary>
	/// If a registered object has moved, recalculate all RayNodes.
	/// </summary>
	protected void OnObjectMoved()
	{
		if (laserOn)
		{
			StopAllCoroutines();
			laserOrigin = transform.position;
			laserDirection = (transform.position - parent.position).normalized;
			root.rayLR.SetPosition(0, transform.position);
			Emit(laserOrigin, laserDirection, layermask, root);
		}

	}

	private void NoBlackHole(RaycastHit rHit, RayNode node)
	{
		Reflect hitReflect = rHit.collider.GetComponent<Reflect>();
		Vector3 h = new Vector3(rHit.point.x, rHit.point.y, 0f);
		if (hitReflect != null)
		{
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			Reflect(node, 0, node.rayLR.GetPosition(0), 1, rHit);
			//StopAllCoroutines();
		}
		else if (rHit.collider.gameObject.tag == "Prism")
		{
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}

			Refract(node, node.rayLR.GetPosition(0), rHit);
			//StopAllCoroutines();
			//Debug.Log("Hit prism");
		}
		else if (rHit.collider.gameObject.GetComponent<Target>() != null)
		{

			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);

			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			node.hitObject = rHit.collider.gameObject;
			OnTargetHit(rHit.collider.gameObject, node);
			//StopAllCoroutines();
			node.PruneWholeTree();
		}
		else if (rHit.collider.gameObject.GetComponent<Filter>() != null)
		{

			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			//StopAllCoroutines();
			HitFilter(node, rHit);
		}
		else if (rHit.collider.gameObject.GetComponent<ChargeObject>() != null)
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			node.hitObject = rHit.collider.gameObject;
			node.PruneWholeTree();
			OnHitCharge(rHit.collider.gameObject, node.rayLR.material);
		}
		else if (rHit.collider.gameObject.GetComponent<Splitter>() != null)
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			HitSplitter(node, rHit);
		}
		else if (rHit.collider.gameObject.tag == "lens")
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			HitLens(node, rHit);
		}
		else if (rHit.collider.tag == "Breakable" && node.intensified)
		{
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			HitBreakable(node, rHit);
		}
		else if (rHit.collider.tag == "Heatable" && node.intensified)
		{
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			HitHeatableObject(node, rHit);
		}
		else
		{
			//we hit something that does not relfect light
			//if (node.hitObject != rHit.collider.gameObject)
			//{
			//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
			//}
			//StopAllCoroutines();
			node.hitObject = rHit.collider.gameObject;
			node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
			node.PruneWholeTree();
		}

		node.HandleMetaBall();
	}

	private void BlackHole(RaycastHit rHit, RayNode node, LayerMask mask)
	{
		//if a black hole is encountered, check if it is contained in the colliderList
		// (collider list should use RayNode as key, then store the black hole)
		//if it is, simply update the children of this node,
		//if it is not, add it and create the child of this node.
		//the next node must be set as the child of the first child
		//the final child must be marked as uninfluenced by the black hole,
		//but the others must be marked as influenced. this will be used to prevent
		// them from recurvely calling checkblackholes
		// call emit for each child
		Vector3 lerpPos = new Vector3();
		Vector3 center = new Vector3();
		float rad = 0;
		//the sphere cast above ignores all colliders except those on the black hole layer
		//now that we know it is possible that this ray of light is influenced by a black hole,
		//this raycast is to check that there is not some other collider inbetween
		RaycastHit doubleCheckHit;
		lerpPos = rHit.point + rHit.normal * blackHoleDistance;
		if (Physics.Raycast(lerpPos, rHit.point - lerpPos, out doubleCheckHit, range, mask))
		{
			if (rHit.collider == doubleCheckHit.collider)
			{

					//Debug.DrawLine(rHit.point, node.rayLR.GetPosition(0), Color.blue);
					rad = rHit.transform.GetComponent<SphereCollider>().radius;
					center = rHit.transform.position;
					node.rayLR.SetPosition(1, lerpPos);
					Vector3 velocity = (node.rayLR.GetPosition(1) - node.rayLR.GetPosition(0)).normalized * speed;
					node.hitObject = null;
					SpawnBlackHoleNodes(rHit.collider.gameObject, node, lerpPos, center, velocity, rad);
			}
		}
	}

	/// <summary>
	/// Main code for handling the emission of a ray of light.
	/// </summary>
	/// <param name="start">location</param>
	/// <param name="direction">normalized direction</param>
	/// <param name="mask">Raycasting mask</param>
	/// <param name="node">RayNode to update</param>
	private void Emit(Vector3 start, Vector3 direction, LayerMask mask, RayNode node)
	{
		RaycastHit pHit;


		RaycastHit hit = new RaycastHit();


		Ray ray;

		ray = new Ray(start, direction);
		bool rEmit = Physics.Raycast(ray, out pHit, range, mask, QueryTriggerInteraction.Ignore);
		bool bEmit = Physics.SphereCast(ray, blackHoleDistance, out hit, range, LayerMask.GetMask("BlackHole"));
		if (rEmit && bEmit)
		{
			if (Vector3.Distance(pHit.point, start) < Vector3.Distance(hit.point, start))
			{
				NoBlackHole(pHit, node);
			}
			else
			{
				Debug.Log("Black hole version");
				BlackHole(hit, node, mask);
			}

		}
		else if (rEmit && !bEmit)
		{
			NoBlackHole(pHit, node);
		}
		else if (!rEmit && bEmit)
		{
			BlackHole(hit, node, mask);
		}
		else
		{
			//StopAllCoroutines();
			node.rayLR.SetPosition(1, node.gameObject.transform.position + direction * range);
			node.hitObject = null;
			node.HandleMetaBall();
			node.PruneWholeTree();
		}
	}

	private bool BHEmit(RayNode node, Vector3 startPosition, Vector3 direction)
	{
		RaycastHit rHit;
		Vector3 h;
		bool hitSomtheing = false;
		float dist = (node.rayLR.GetPosition(1) - node.rayLR.GetPosition(0)).magnitude;
		if (Physics.Raycast(startPosition, direction, out rHit, dist))
		{
			Reflect hitReflect = rHit.collider.GetComponent<Reflect>();
			h = new Vector3(rHit.point.x, rHit.point.y, 0f);
			hitSomtheing = true;
			if (hitReflect != null)
			{
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				Reflect(node, 0, node.rayLR.GetPosition(0), 1, rHit);
				
			}
			else if (rHit.collider.gameObject.name == "prism")
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}

				Refract(node, node.rayLR.GetPosition(0), rHit);
				//Debug.Log("Hit prism");
			}
			else if (rHit.collider.gameObject.GetComponent<Target>() != null)
			{

				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);

				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				node.hitObject = rHit.collider.gameObject;
				OnTargetHit(rHit.collider.gameObject, node);
				node.PruneWholeTree();
			}
			else if (rHit.collider.gameObject.GetComponent<Filter>() != null)
			{

				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				HitFilter(node, rHit);
			}
			else if (rHit.collider.gameObject.GetComponent<ChargeObject>() != null)
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				node.hitObject = rHit.collider.gameObject;
				node.PruneWholeTree();
				OnHitCharge(rHit.collider.gameObject, node.rayLR.material);
			}
			else if (rHit.collider.gameObject.GetComponent<Splitter>() != null)
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				HitSplitter(node, rHit);
			}
			else if (rHit.collider.gameObject.tag == "lens")
			{
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				HitLens(node, rHit);
			}
			else if (rHit.collider.tag == "Breakable" && node.intensified)
			{
				HitBreakable(node, rHit);
			}
			else
			{
				//we hit something that does not relfect light
				//if (node.hitObject != rHit.collider.gameObject)
				//{
				//	FMODUnity.RuntimeManager.PlayOneShot(Ping);
				//}
				Debug.Log(node.gameObject.name + " has hit " + rHit.collider.name);
				node.hitObject = rHit.collider.gameObject;
				node.rayLR.SetPosition(node.rayLR.positionCount - 1, h);
				node.PruneWholeTree();
			}
			
		}
		return hitSomtheing;
		//we do not change the ending position in blackhole emit because we want it to stay the same
		//as set by the blackhole calculations

	}

	private void HitBreakable(RayNode node, RaycastHit rHit)
	{
		//StopAllCoroutines();
		Vector3 original = node.rayLR.GetPosition(1);
		node.rayLR.SetPosition(1, rHit.point);
		node.hitObject = rHit.collider.gameObject;
		RaycastHit[] hits = Physics.SphereCastAll(node.rayLR.GetPosition(0), .15f, (original - node.rayLR.GetPosition(0)).normalized,
		 range, burnableLayer);
		if (hits.Length == 1 && hits[0].collider.gameObject.name == "Icosphere")
		{
			node.hitObject.GetComponentInParent<Breakable>().Spawn(node.hitObject.transform);
			node.hitObject.GetComponentInParent<Breakable>().DestroyStuff(node.hitObject.transform);
			hits = Physics.SphereCastAll(node.rayLR.GetPosition(0), .1f, (original - node.rayLR.GetPosition(0)).normalized,
			range, burnableLayer);
			//this should up them in increasing order of distance
			hits = hits.OrderBy(hit => hit.distance).ToArray();
			StartCoroutine(HitBreak(node, hits, original, rHit.point));
		}
		else
		{
			hits = hits.OrderBy(hit => hit.distance).ToArray();
			StartCoroutine(HitBreak(node, hits, original, rHit.point));
		}
	}

	IEnumerator HitBreak(RayNode node, RaycastHit[] rHit, Vector3 original, Vector3 oH)
	{
		for (int i = 0; i < rHit.Length; i++)
		{
			if (rHit[i].collider != null)
			{
				if (rHit[i].collider.gameObject.name == "Icosphere")
				{
					rHit[i].collider.GetComponentInParent<Breakable>().DestroyStuff(rHit[i].collider.transform);
				}
				rHit[i].collider.GetComponentInParent<Breakable>().Spawn(rHit[i].collider.transform);
				if (node.rayLR != null)
				{
					Vector3 p = oH + (original - node.rayLR.GetPosition(0)).normalized * .01f *i;
					p.z = 0f;
					node.rayLR.SetPosition(1, p);
				}
				else
				{
					StopAllCoroutines();
				}

				while (timer > 0)
				{
					timer -= Time.deltaTime;
					yield return null;
				}
				timer = timerMax;

				if (rHit[i].collider != null)
				{
					Destroy(rHit[i].collider.gameObject);
				}

			}

		}

		if (node.rayLR != null)
		{
			Vector3 pos = node.rayLR.GetPosition(0) + (original - node.rayLR.GetPosition(0)).normalized * range;
			node.rayLR.SetPosition(1, pos);
			Emit(node.rayLR.GetPosition(0), (node.rayLR.GetPosition(1) - node.rayLR.GetPosition(0)).normalized, layermask, node);
		}
		else
		{
			StopAllCoroutines();
		}
		
		
	}

	/// <summary>
	/// Main code for calculating the position of raynodes under the influence of black holes
	/// </summary>
	/// <param name="blackHole"></param>
	/// <param name="parent"></param>
	/// <param name="startPosition"></param>
	/// <param name="center"></param>
	/// <param name="velocity"></param>
	/// <param name="rad"></param>
	private void SpawnBlackHoleNodes(GameObject blackHole, RayNode parent, Vector3 startPosition, Vector3 center, Vector3 velocity, float rad)
	{
		List<Vector3> tempList = new List<Vector3>();
		tempList.Add(startPosition);
		bool hitHole = false;
		Vector3 gravityVec;
		float d;
		Vector3 accel = Vector3.zero;
		Vector3 previous = startPosition;
		float time = 0.01f;
		for (int i = 0; i < 150; i++)
		{

			gravityVec = center - previous;
			gravityVec.z = 0f;
			d = Vector3.Distance(center, previous);
			accel.x = blackHoleMass / (d * d * d * d * d);
			accel.y = blackHoleMass / (d * d * d * d * d);
			accel = Vector3.Scale(accel, gravityVec);
			velocity += accel * time;
			previous += velocity * time;

			if (i % 4 == 0)
			{
				tempList.Add(previous);
			}
			if (Vector3.Distance(previous, center) < rad)
			{
				tempList.Add(previous);
				hitHole = true;
				break;
			}
			else if (Vector3.Distance(previous, center) > blackHoleDistance + rad)
			{
				tempList.Add(previous);
				break;
			}
		}

		GameObject par = parent.gameObject;
		GameObject go;
		parent.PruneWholeTree();

		for (int i = 0; i < tempList.Count; i++)
		{
			if (i + 1 < tempList.Count)
			{
				if (!hitHole)
				{
					go = SetUpRayNodeGO(par.GetComponent<RayNode>().rayLR.materials, tempList[i], tempList[i + 1],
					par.GetComponent<RayNode>().depth, parent.rayLR.widthCurve, true, parent.intensified, "BH" + i);
					par.GetComponent<RayNode>().children.Add(go.GetComponent<RayNode>());
					par = go;
					if (BHEmit(go.GetComponent<RayNode>(), tempList[i], (tempList[i + 1] - tempList[i]).normalized))
					{
						break;
					}
					
				}
				else if (hitHole && Vector3.Distance(tempList[i + 1], center) < rad)
				{
					go = SetUpRayNodeGO(par.GetComponent<RayNode>().rayLR.materials, tempList[i], center,
					par.GetComponent<RayNode>().depth, parent.rayLR.widthCurve, true, parent.intensified, "BH" + i);
					par.GetComponent<RayNode>().children.Add(go.GetComponent<RayNode>());
					go.GetComponent<RayNode>().hitObject = blackHole;
					go.name = "BHCollision";
					break;
				}
				else
				{
					go = SetUpRayNodeGO(par.GetComponent<RayNode>().rayLR.materials, tempList[i], tempList[i + 1],
					par.GetComponent<RayNode>().depth, parent.rayLR.widthCurve, true, parent.intensified, "BH" + i);
					par.GetComponent<RayNode>().children.Add(go.GetComponent<RayNode>());
					if (BHEmit(go.GetComponent<RayNode>(), tempList[i], (tempList[i + 1] - tempList[i]).normalized))
					{
						break;
					}
					par = go;
				}

			}
			else
			{
				if (!hitHole)
				{
					Vector3 n = (tempList[i] - tempList[i - 1]).normalized;
					if (Vector3.Magnitude(n) < .1f)
					{
						n = (tempList[i] - tempList[i - 2]).normalized;
					}
					par.GetComponent<RayNode>().rayLR.SetPosition(0, tempList[i - 1]);
					par.GetComponent<RayNode>().rayLR.SetPosition(1, tempList[i - 1] +
						n * range);
					par.GetComponent<RayNode>().influencedByBlackHole = false;
					par.name = "Last";
					Emit(par.GetComponent<RayNode>().rayLR.GetPosition(0), (par.GetComponent<RayNode>().rayLR.GetPosition(1) -
						par.GetComponent<RayNode>().rayLR.GetPosition(0)).normalized, layermask, par.GetComponent<RayNode>());
					break;
				}
				else if (hitHole && Vector3.Distance(tempList[i], center) < rad)
				{
					par.GetComponent<RayNode>().rayLR.SetPosition(1, center);
					par.GetComponent<RayNode>().hitObject = blackHole;
					par.GetComponent<RayNode>().PruneWholeTree();
					par.name = "BHCollision";
					break;
				}
			}
		}
	}


	private GameObject SetUpRayNodeGO(Material[] mats, Vector3 start, Vector3 end, int parentDepth, AnimationCurve curve, bool influencedByBlackHole, bool intensified, string name = "")
	{
		start.z = 0f;
		end.z = 0f;
		GameObject go = new GameObject();
		go.name = name;
		go.transform.position = start;
		RayNode rn = go.AddComponent<RayNode>();
		LineRenderer lr = rn.rayLR = go.AddComponent<LineRenderer>();


		Vector3 dir = end - start;
		GameObject meta = Instantiate(metaball, (start + end) / 2f, Quaternion.identity);
		meta.transform.parent = go.transform;
		rn.metaball = meta;
		//meta.transform.position = start + (dir * Vector3.Distance(start, end) / 2f);
		meta.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg, Vector3.forward);
		meta.transform.localScale = new Vector3(2f* Vector3.Distance(start, end), 2f, 1f);

		lr.textureMode = LineTextureMode.DistributePerSegment;
		lr.materials = mats;
		rn.depth = parentDepth;// + 1;
		lr.widthCurve = curve;
		lr.sortingOrder = 1; //this makes the linerenderer display on top
		rn.rayLR.positionCount = 2;
		rn.rayLR.SetPosition(0, start);
		rn.rayLR.SetPosition(1, end);
		rn.influencedByBlackHole = influencedByBlackHole;
		rn.intensified = intensified;
		return go;
	}

	private void HitLens(RayNode ray, RaycastHit hit)
	{

		GameObject refractionGO;
		Vector3 h = new Vector3(hit.point.x, hit.point.y, 0f);
		Vector3 LBase = (h - ray.rayLR.GetPosition(0)).normalized;
		float CBase = -Vector3.Dot(hit.normal, LBase);
		if (CBase < 0)
		{
			CBase = -CBase;
		}
		Vector3 enter = GetRefractionEnterPosition(3, hit.normal, LBase, CBase);
		enter += h;
		if ((hit.collider.gameObject != ray.hitObject && ray.hitObject != null) || ray.hitObject == null)
		{
			ray.PruneWholeTree();
			ray.hitObject = hit.collider.gameObject;
			refractionGO = SetUpRayNodeGO(ray.rayLR.materials, h, enter, ray.depth + 1, ray.rayLR.widthCurve,
				ray.influencedByBlackHole, true, "Lens");
			ray.children.Add(refractionGO.GetComponent<RayNode>());
			LensCalculations(refractionGO.GetComponent<RayNode>(), 3);

		}
		else
		{
			refractionGO = ray.children[0].gameObject;
			refractionGO.transform.position = h;
			refractionGO.GetComponent<RayNode>().rayLR.SetPosition(0, h);
			refractionGO.GetComponent<RayNode>().rayLR.SetPosition(1, enter);
			LensCalculations(refractionGO.GetComponent<RayNode>(), 3);
		}


	}

	private void HitFilter(RayNode ray, RaycastHit hit)
	{
		bool found = false;
		RayNode child;
		GameObject go;
		RaycastHit sHit;
		Vector3 h = new Vector3(hit.point.x, hit.point.y, 0f);
		Vector3 newPos = h + (h - ray.rayLR.GetPosition(0)).normalized * range;
		newPos.z = 0f;


		Vector3 otherSide = newPos;
		Material[] mats = hit.collider.GetComponent<Filter>().materialsToPass;
		if (Physics.Raycast(newPos, h - newPos, out sHit, range, filterRaycastLayerMask, QueryTriggerInteraction.Ignore))
		{
			if (sHit.collider.gameObject == hit.collider.gameObject)
			{
				Vector3 o = new Vector3(sHit.point.x, sHit.point.y, 0f);
				otherSide = o;
			}
		}
		if (ray.rayLR.material.color == WhiteLight.color)
		{
			found = true;
			for (int i = 0; i < mats.Length; i++)
			{
				if ((ray.hitObject != null && ray.hitObject != hit.collider.gameObject) || ray.hitObject == null)
				{
					ray.hitObject = hit.collider.gameObject;
					ray.PruneWholeTree();
					Material[] ms = new Material[] { mats[i], ray.rayLR.materials[1], ray.rayLR.materials[2] };
					go = SetUpRayNodeGO(ms, otherSide, newPos, ray.depth, ray.rayLR.widthCurve, ray.influencedByBlackHole, ray.intensified, "Filter");
					ray.children.Add(go.GetComponent<RayNode>());
					Vector3 dir = (otherSide - h).normalized;
					Emit(otherSide, dir, layermask, go.GetComponent<RayNode>());
				}
				else if (ray.hitObject == hit.collider.gameObject)
				{
					ray.hitObject = hit.collider.gameObject;
					child = ray.children.First(r => r.rayLR.material.color == mats[i].color);
					go = child.gameObject;
					go.transform.position = otherSide;
					child.rayLR.SetPosition(0, otherSide);
					Vector3 dir = (otherSide - hit.point).normalized;
					Emit(otherSide, dir, layermask, child);
				}
			}
		}
		else
		{
			for (int i = 0; i < mats.Length; i++)
			{
				if (ray.rayLR.material.color == mats[i].color)
				{
					found = true;
					if ((ray.hitObject != null && ray.hitObject != hit.collider.gameObject) || ray.hitObject == null)
					{
						ray.hitObject = hit.collider.gameObject;
						ray.PruneWholeTree();
						Material[] ms = new Material[] { mats[i], ray.rayLR.materials[1], ray.rayLR.materials[2] };
						go = SetUpRayNodeGO(ms, otherSide, newPos, ray.depth, ray.rayLR.widthCurve, ray.influencedByBlackHole, ray.intensified, "Filter");
						ray.children.Add(go.GetComponent<RayNode>());
						Vector3 dir = (otherSide - h).normalized;
						Emit(otherSide, dir, layermask, go.GetComponent<RayNode>());
					}
					else if (ray.hitObject == hit.collider.gameObject)
					{
						ray.hitObject = hit.collider.gameObject;
						child = ray.children.First(r => r.rayLR.material.color == mats[i].color);
						go = child.gameObject;
						go.transform.position = otherSide;
						child.rayLR.SetPosition(0, otherSide);
						Vector3 dir = (otherSide - hit.point).normalized;
						Emit(otherSide, dir, layermask, child);
					}
				}
			}

			if (!found)
			{
				ray.hitObject = hit.collider.gameObject;
				ray.PruneWholeTree();
			}
		}

	}

	private void HitHeatableObject(RayNode ray, RaycastHit hit)
	{
		ray.hitObject = hit.collider.gameObject;
		StartCoroutine(HitHeatable(ray.hitObject,ray.rayLR.GetPosition(0),hit));
	}

	IEnumerator HitHeatable(GameObject go, Vector3 start, RaycastHit hit)
	{
		while (true)
		{
			go.GetComponent<HeatEffect>().Heat(start,hit);
			yield return null;
		}
	}

	private void HitSplitter(RayNode ray, RaycastHit hit)
	{
		RayNode reflectChild;
		GameObject reflectGO;
		RayNode passChild;
		GameObject passGO;
		RaycastHit sHit;
		Vector3 incidence, newPos;
		GameObject hitObject = hit.collider.gameObject;

		//perform angle and vector calculations
		incidence = (hit.point - ray.rayLR.GetPosition(0)).normalized;
		newPos = incidence - 2 * (Vector3.Dot(incidence, hit.normal)) * hit.normal;
		newPos = newPos.normalized;
		Vector3 passPos = hit.point + (hit.point - ray.rayLR.GetPosition(0)).normalized * range;


		Vector3 otherSide = passPos;
		if (Physics.Raycast(hit.point + (hit.point - ray.rayLR.GetPosition(0)).normalized * .55f, hit.point - passPos,
			out sHit, range, layermask, QueryTriggerInteraction.Ignore))
		{
			if (sHit.collider.gameObject == hit.collider.gameObject)
			{
				otherSide = sHit.point;
			}
		}

		ray.rayLR.SetPosition(1, hit.point);

		//get materials to split
		Splitter split = hit.collider.gameObject.GetComponent<Splitter>();
		List<Material> mats = new List<Material>();
		if (ray.rayLR.material.color == WhiteLight.color)
		{
			for (int i = 0; i < split.targetMaterials.Length; i++)
			{
				mats.Add(split.targetMaterials[i]);
			}
		}
		else if (split.targetMaterials.Length == 1 && split.targetMaterials[0].color == WhiteLight.color)
		{
			mats.Add(ray.rayLR.material);
		}




		if ((ray.hitObject != hitObject && ray.hitObject != null) || ray.hitObject == null || ray.hitObject == hitObject)
		{
			ray.hitObject = hitObject;
			ray.PruneWholeTree();

			for (int i = 0; i < mats.Count; i++)
			{
				//set up raynode that passes through the splitter
				passGO = new GameObject("Pass");
				passGO.transform.position = hit.point;
				passChild = passGO.AddComponent<RayNode>();
				passChild.rayLR = passGO.AddComponent<LineRenderer>();
				passChild.depth = ray.depth + 1;
				passChild.rayLR.widthCurve = new AnimationCurve(new Keyframe(0, curve[0].value /
					(float)passChild.depth), new Keyframe(1, curve[1].value / (float)passChild.depth));
				Material[] ms = new Material[] { mats[i], ray.rayLR.materials[1], ray.rayLR.materials[2] };
				passChild.rayLR.materials = ms;
				passChild.rayLR.positionCount = 2;
				passChild.rayLR.SetPosition(0, otherSide);
				ray.children.Add(passChild);
				Vector3 dir = (otherSide - hit.point).normalized;
				Emit(otherSide, dir, layermask, passChild);

				//set up raynode that reflects off the splitter
				reflectGO = new GameObject("Reflect");
				reflectGO.transform.position = hit.point;
				reflectChild = reflectGO.AddComponent<RayNode>();
				reflectChild.depth = ray.depth + 1;
				reflectChild.rayLR = reflectGO.AddComponent<LineRenderer>();
				reflectChild.rayLR.widthCurve = new AnimationCurve(new Keyframe(0, curve[0].value /
					(float)reflectChild.depth), new Keyframe(1, curve[1].value / (float)reflectChild.depth));
				reflectChild.rayLR.materials = ms;
				reflectChild.rayLR.positionCount = 2;
				reflectChild.rayLR.SetPosition(0, hit.point);
				ray.children.Add(reflectChild);



				Emit(hit.point, newPos, layermask, reflectChild);
			}


		}
	}


	private void OnHitCharge(GameObject charge, Material material)
	{
		//charge.GetComponent<ChargeObject>().OnHitByTargetMaterial(material);
		StartCoroutine(Charging(charge, material));
	}

	IEnumerator Charging(GameObject go, Material mat)
	{
		while (true)
		{
			go.GetComponent<ChargeObject>().OnHitByTargetMaterial(mat);
			yield return null;
		}
	}

	/// <summary>
	/// Simply returns the materials to reflect based on the linerenderer material and the object
	/// </summary>
	/// <param name="lr">LineRenderer which hit an object</param>
	/// <param name="reflect">Reflect component that contains the materials to be reflected</param>
	/// <returns></returns>
	private Material[] GetMaterialsToReflect(LineRenderer lr, Reflect reflect)
	{
		List<Material> materialToReflect = new List<Material>();
		//the light hitting the object is not white, and the object does not reflect white light
		if (lr.material.color != WhiteLight.color && reflect.materialsToReflect.Length >= 1
			&& reflect.materialsToReflect[0].color != WhiteLight.color)
		{
			//Debug.Log("Checking individual materials");
			for (int i = 0; i < reflect.materialsToReflect.Length; i++)
			{
				if (reflect.materialsToReflect[i].color == lr.material.color)
				{
					materialToReflect.Add(lr.material);
				}
			}

		}
		else if (reflect.materialsToReflect.Length == 1 && reflect.materialsToReflect[0].color == WhiteLight.color)
		{
			//the light is not white, but the object is
			materialToReflect.Add(lr.material);
		}
		else
		{
			//the light is white, but the object is not
			materialToReflect = reflect.materialsToReflect.ToList();
		}
		return materialToReflect.ToArray();
	}

	/// <summary>
	/// Updates and calculates reflections
	/// </summary>
	/// <param name="go">GameObject which holds the LineRenderer component</param>
	/// <param name="startIndex">starting index of LineRenderer from which calculations will begin</param>
	/// <param name="startLocation">3D location at starting index</param>
	/// <param name="hitIndex">index of hit</param>
	/// <param name="hit">Raycast hit which contains all the hit data</param>
	/// 
	private void Reflect(RayNode r, int startIndex, Vector3 startLocation, int hitIndex, RaycastHit hit)
	{

		LineRenderer lr = r.rayLR;
		Vector3 incidence, newPos;
		Vector3 target = Vector3.zero;
		Vector3 h = new Vector3(hit.point.x, hit.point.y, 0f);
		GameObject hitObject = hit.collider.gameObject;

		//set the starting and hit locations
		startLocation.z = 0;
		lr.SetPosition(startIndex, startLocation);
		lr.SetPosition(hitIndex, h);
		if (r.hitObject != hitObject && r.hitObject != null)
		{
			r.hitObject = hitObject;
			r.PruneWholeTree();
		}
		else if (r.hitObject == hitObject)
		{
			r.hitObject = hitObject;
		}
		else
		{
			r.PruneWholeTree();
			r.hitObject = hitObject;
		}

		//perform angle and vector calculations
		incidence = (h - startLocation).normalized;
		newPos = GetReflectionPosition(incidence, hit.normal);



		Reflect hitReflect = hit.collider.GetComponent<Reflect>();
		if (hitReflect != null)
		{
			////make sure we should be reflecting
			Material[] matsToReflect = GetMaterialsToReflect(lr, hitReflect);
			if (matsToReflect.Length > 0)
			{

				for (int i = 0; i < matsToReflect.Length; i++)
				{
					GameObject reflectionGO;
					LineRenderer reflectionLR;
					if (r.children.Count == 0)
					{
						Material[] ms = new Material[] { matsToReflect[i], r.rayLR.materials[1], r.rayLR.materials[2] };
						reflectionGO = SetUpRayNodeGO(ms, h, newPos * range, r.depth + 1, r.rayLR.widthCurve,
							r.influencedByBlackHole, r.intensified, "Reflection " + hitObject.name);
						r.children.Add(reflectionGO.GetComponent<RayNode>());
					}
					else if (r.children.Count == 1)
					{
						reflectionGO = r.children[0].gameObject;
						reflectionLR = reflectionGO.GetComponent<LineRenderer>();
						r.children[0].gameObject.transform.position = h;
						reflectionLR.SetPosition(0, h);
						reflectionLR.SetPosition(1, newPos * range);
						reflectionGO.GetComponent<RayNode>().HandleMetaBall();

					}
					else
					{
						reflectionGO = r.children[0].gameObject;
						reflectionLR = reflectionGO.GetComponent<LineRenderer>();
						r.children[0].gameObject.transform.position = h;
						reflectionLR.SetPosition(0, h);
						reflectionLR.SetPosition(1, newPos * range);
						reflectionGO.GetComponent<RayNode>().HandleMetaBall();
					}


					r.children[0].gameObject.transform.position = h;
					Emit(reflectionGO.GetComponent<RayNode>().rayLR.GetPosition(0), newPos, layermask, r.children[0]);
				}
			}
		}

	}

	private void LensCalculations(RayNode refractionGO, int i)
	{
		//Debug.Log(refractionGO.gameObject.name);
		RaycastHit hit;
		GameObject refractionChild;
		Vector3 exitPos;
		Vector3 h;
		Vector3 start = refractionGO.rayLR.GetPosition(1) + (refractionGO.rayLR.GetPosition(1) - refractionGO.rayLR.GetPosition(0)).normalized * 2f;
		if (Physics.Raycast(start, (refractionGO.rayLR.GetPosition(0) - refractionGO.rayLR.GetPosition(1)).normalized,
						out hit, range, lensRaycastLayerMask, QueryTriggerInteraction.Ignore))
		{
			h = new Vector3(hit.point.x, hit.point.y, 0);
			Vector3 L = (h - refractionGO.rayLR.GetPosition(0)).normalized;
			float C = -Vector3.Dot(-hit.normal, L);
			if (C < 0)
			{
				C = -C;
			}
			exitPos = GetRefractionExitPosition(i, -hit.normal, L, C);
			exitPos *= range;
			exitPos += h;
			exitPos.z = 0f;

			if ((refractionGO.hitObject != null && refractionGO.hitObject != hit.collider.gameObject) || refractionGO.hitObject == null)
			{
				refractionGO.PruneWholeTree();
				AnimationCurve c = new AnimationCurve(new Keyframe(0f, refractionGO.rayLR.widthCurve.keys[0].value * 2),
				new Keyframe(1f, refractionGO.rayLR.widthCurve.keys[1].value * 2));
				refractionChild = SetUpRayNodeGO(refractionGO.rayLR.materials, h, exitPos,
					refractionGO.depth + 1, c, refractionGO.influencedByBlackHole, true, "Refraction" + i);
				refractionGO.hitObject = hit.collider.gameObject;
				refractionGO.children.Add(refractionChild.GetComponent<RayNode>());
				refractionGO.rayLR.SetPosition(1, h);

			}
			else
			{
			//	//we already hit it, so update children
				refractionChild = refractionGO.children[0].gameObject;
				refractionChild.transform.position = h;
				refractionGO.rayLR.SetPosition(1, h);
				refractionChild.GetComponent<RayNode>().rayLR.SetPosition(0, refractionGO.rayLR.GetPosition(1));
				refractionChild.GetComponent<RayNode>().rayLR.SetPosition(1, exitPos);
			}

			Emit(refractionGO.rayLR.GetPosition(1), (exitPos - refractionGO.rayLR.GetPosition(1)).normalized, layermask, refractionChild.GetComponent<RayNode>());
		}
		else
		{
			//Debug.Log("Missed");
			////refractionGO.PruneWholeTree();
			//Emit(refractionGO.rayLR.GetPosition(0), (refractionGO.rayLR.GetPosition(0) - refractionGO.rayLR.GetPosition(0)).normalized, layermask, refractionGO);
		}
	}

	/// <summary>
	/// Performs some repetitive calculations involving the refraction of light
	/// </summary>
	/// <param name="refractionGO">the game object holding the LineRenderer</param>
	/// <param name="LBase">Normalized vector holding the direction from the light source to the hit point</param>
	/// <param name="CBase"></param>
	/// <param name="newPos">the newly calculated positon</param>
	/// <param name="startLocation">starting location for the light source</param>
	/// <param name="Hit">RaycastHit that stores hit location and normal vector</param>
	/// <param name="i">refraction index</param>

	private void RefractionCalculations(RayNode refractionGO, int i)
	{
		RaycastHit hit;
		RaycastHit secondaryHit;
		GameObject refractionChild;
		Vector3 exitPos;
		Vector3 h;
		Vector3 secondH;
		Vector3 secondExit;

		Vector3 start = refractionGO.rayLR.GetPosition(1) + (refractionGO.rayLR.GetPosition(1) - refractionGO.rayLR.GetPosition(0)).normalized * 2f;
		if (Physics.Raycast(start, (refractionGO.rayLR.GetPosition(0) - refractionGO.rayLR.GetPosition(1)).normalized,
						out hit, range, prismRaycastLayerMask, QueryTriggerInteraction.Ignore))
		{
			h = new Vector3(hit.point.x, hit.point.y, 0);
			Vector3 L = (h - refractionGO.rayLR.GetPosition(0)).normalized;
			float C = -Vector3.Dot(-hit.normal, L);
			if (C < 0)
			{
				C = -C;
			}

			//for debugging
			float R = (1.1f + (i * 0.05f)) / 1f;
			float x = 1 - (R * R * (1 - (C * C)));
			if (x < 0)
			{
				exitPos = GetReflectionPosition(L, hit.normal);
				exitPos += (exitPos - h).normalized * 2f;
				exitPos += h;
				if ((refractionGO.hitObject != null && refractionGO.hitObject != hit.collider.gameObject) || refractionGO.hitObject == null)
				{
					refractionGO.PruneWholeTree();
					//RaycastHit again to find the hit point, then emit
					if (Physics.Raycast(exitPos,(h-exitPos).normalized,out secondaryHit,range,prismRaycastLayerMask,QueryTriggerInteraction.Ignore))
					{
						secondH = new Vector3(secondaryHit.point.x, secondaryHit.point.y, 0f);
						L = (secondH - exitPos).normalized;
						C = -Vector3.Dot(-secondaryHit.normal, L);
						if (C < 0)
						{
							C = -C;
						}
						secondExit = GetRefractionExitPosition(i, -secondaryHit.normal, L, C);
						secondExit *= range;
						secondExit += exitPos;

						refractionGO.rayLR.SetPosition(1, h);
						refractionChild = SetUpRayNodeGO(refractionGO.rayLR.materials, h, secondH,
						refractionGO.depth + 1, refractionGO.rayLR.widthCurve, refractionGO.influencedByBlackHole, refractionGO.intensified, "Refraction" + i);
						refractionGO.children.Add(refractionChild.GetComponent<RayNode>());
						GameObject secondRefractionChild = SetUpRayNodeGO(refractionGO.rayLR.materials, secondH, secondExit, refractionGO.depth + 2,
							refractionGO.rayLR.widthCurve, refractionGO.influencedByBlackHole, refractionGO.intensified, "Refraction" + i);
						refractionChild.GetComponent<RayNode>().hitObject = secondaryHit.collider.gameObject;
						refractionChild.GetComponent<RayNode>().children.Add(secondRefractionChild.GetComponent<RayNode>());

						RefractionCalculations(secondRefractionChild.GetComponent<RayNode>(), i);
					}
				}
				else
				{
					//we already hit it, so update children
					refractionChild = refractionGO.children[0].gameObject;
					refractionChild.transform.position = h;
					refractionGO.rayLR.SetPosition(1, h);

					if (Physics.Raycast(exitPos, (h - exitPos).normalized, out secondaryHit, range, prismRaycastLayerMask, QueryTriggerInteraction.Ignore))
					{
						secondH = new Vector3(secondaryHit.point.x, secondaryHit.point.y, 0f);
						//set positions for first child
						refractionChild.GetComponent<RayNode>().rayLR.SetPosition(0, h);
						refractionChild.GetComponent<RayNode>().rayLR.SetPosition(1, secondH);

						L = (secondH - exitPos).normalized;
						C = -Vector3.Dot(-secondaryHit.normal, L);
						if (C < 0)
						{
							C = -C;
						}
						secondExit = GetRefractionExitPosition(i, -secondaryHit.normal, L, C);
						secondExit *= range;
						secondExit += exitPos;
						GameObject secondRefractionChild = refractionChild.GetComponent<RayNode>().children[0].gameObject;
						//set positions for second child
						secondRefractionChild.transform.position = secondH;
						secondRefractionChild.GetComponent<RayNode>().rayLR.SetPosition(0, secondH);
						secondRefractionChild.GetComponent<RayNode>().rayLR.SetPosition(1, secondExit);
						RefractionCalculations(secondRefractionChild.GetComponent<RayNode>(), i);
					}
				}

				
			}
			else
			{
				//we exited the prism regularly
				exitPos = GetRefractionExitPosition(i, -hit.normal, L, C);
				exitPos *= range;
				exitPos += h;
				exitPos.z = 0f;
				if ((refractionGO.hitObject != null && refractionGO.hitObject != hit.collider.gameObject) || refractionGO.hitObject == null)
				{
					refractionGO.PruneWholeTree();

					refractionChild = SetUpRayNodeGO(refractionGO.rayLR.materials, h, exitPos,
						refractionGO.depth + 1, refractionGO.rayLR.widthCurve, refractionGO.influencedByBlackHole, refractionGO.intensified, "Refraction" + i);
					refractionGO.children.Add(refractionChild.GetComponent<RayNode>());
					refractionGO.rayLR.SetPosition(1, h);

				}
				else
				{
					//we already hit it, so update children
					refractionChild = refractionGO.children[0].gameObject;
					refractionChild.transform.position = h;
					refractionGO.rayLR.SetPosition(1, h);
					refractionChild.GetComponent<RayNode>().rayLR.SetPosition(0, refractionGO.rayLR.GetPosition(1));
					refractionChild.GetComponent<RayNode>().rayLR.SetPosition(1, exitPos);
				}

				Emit(refractionGO.rayLR.GetPosition(1), (exitPos - refractionGO.rayLR.GetPosition(1)).normalized, layermask, refractionChild.GetComponent<RayNode>());
			}	
		}
	}


	/// <summary>
	/// Recursive function that deals with spawning and updating refractions
	/// </summary>
	/// <param name="lightObject">object that hold the LineRenderer</param>
	/// <param name="startLocation">starting position of the light</param>
	/// <param name="Hit">RaycastHit that stores hit location and normal vector</param>
	private void Refract(RayNode lightObject, Vector3 startLocation, RaycastHit Hit)
	{
		//Debug.Log(lightObject.name + " Called refract");
		LineRenderer light = lightObject.rayLR;
		GameObject hitObject = Hit.collider.gameObject;

		Vector3 LBase = (Hit.point - startLocation).normalized;
		LBase.z = 0;
		Vector3 h = new Vector3(Hit.point.x, Hit.point.y, 0f);
		float CBase = -Vector3.Dot(Hit.normal, LBase);
		if (CBase < 0)
		{
			CBase = -CBase;
		}

		Vector3 l = (startLocation - Hit.point).normalized;
		Vector3 enter;


		GameObject refractionGO;
		if ((hitObject != lightObject.hitObject && lightObject.hitObject != null) || lightObject.hitObject == null)
		{
			lightObject.PruneWholeTree();
			lightObject.hitObject = hitObject;
			if (lightObject.rayLR.material.color == WhiteLight.color)
			{
				for (int i = 0; i < refractionMaterials.Length; i++)
				{
					enter = GetRefractionEnterPosition(i, Hit.normal, LBase, CBase);
					enter += h;
					enter.z = 0;
					Material[] m = new Material[3] { refractionMaterials[i], light.materials[1], light.materials[2] };
					refractionGO = SetUpRayNodeGO(m, h, enter, lightObject.depth + 1, light.widthCurve,
						lightObject.influencedByBlackHole, lightObject.intensified, "Refraction" + i);
					lightObject.children.Add(refractionGO.GetComponent<RayNode>());
					RefractionCalculations(refractionGO.GetComponent<RayNode>(), i);
				}
			}
			else
			{
				int index = -1;
				for (int i = 0; i < refractionMaterials.Length; i++)
				{
					if (refractionMaterials[i].color == light.material.color)
					{
						index = i;
						break;
					}
				}

				if (index > -1)
				{
					enter = GetRefractionEnterPosition(index, Hit.normal, LBase, CBase);
					enter += h;
					enter.z = 0f;
					Material[] ms = new Material[] { refractionMaterials[index], lightObject.rayLR.materials[1], lightObject.rayLR.materials[2] };
					refractionGO = SetUpRayNodeGO(ms, h, enter, lightObject.depth + 1, light.widthCurve,
						lightObject.influencedByBlackHole, lightObject.intensified, "Refraction" + index);
					lightObject.children.Add(refractionGO.GetComponent<RayNode>());
					RefractionCalculations(refractionGO.GetComponent<RayNode>(), index);
				}
			}
		}
		else
		{
			//we hit this one before, just update the children
			if (lightObject.rayLR.material.color == WhiteLight.color)
			{
				for (int i = 0; i < refractionMaterials.Length; i++)
				{
					enter = GetRefractionEnterPosition(i, Hit.normal, LBase, CBase);
					enter += h;
					enter.z = 0;
					refractionGO = lightObject.children[i].gameObject;
					refractionGO.transform.position = h;
					refractionGO.GetComponent<RayNode>().rayLR.SetPosition(0, h);
					refractionGO.GetComponent<RayNode>().rayLR.SetPosition(1, enter);
					RefractionCalculations(refractionGO.GetComponent<RayNode>(), i);
				}
			}
			else
			{
				int index = -1;
				for (int i = 0; i < refractionMaterials.Length; i++)
				{
					if (refractionMaterials[i].color == light.material.color)
					{
						index = i;
						break;
					}
				}

				if (index > -1)
				{
					enter = GetRefractionEnterPosition(index, Hit.normal, LBase, CBase);
					enter += h;
					enter.z = 0f;
					refractionGO = lightObject.children[0].gameObject;
					refractionGO.transform.position = h;
					refractionGO.GetComponent<RayNode>().rayLR.SetPosition(0, h);
					refractionGO.GetComponent<RayNode>().rayLR.SetPosition(1, enter);
					RefractionCalculations(refractionGO.GetComponent<RayNode>(), index);
				}
			}

		}
	}


	/// <summary>
	/// Calculates the internal position of the refraction vector
	/// </summary>
	/// <param name="i">refraction index</param>
	/// <param name="surfaceNormal">vector of the surface normal</param>
	/// <param name="L">light Vector</param>
	/// <param name="C"></param>
	/// <returns>the location of the refraction vector</returns>
	private Vector3 GetRefractionEnterPosition(int i, Vector3 surfaceNormal, Vector3 L, float C)
	{
		float R = 1f / (1.1f + (i * 0.05f));
		Vector3 newPos = (R * L) +
				(((R * C) - Mathf.Sqrt(1 - (R * R * (1 - (C * C))))) * surfaceNormal);
		//Debug.Log("new pos " + newPos);
		newPos.Normalize();
		//newPos *= range;
		newPos.z = 0f;
		return newPos;

	}

	private Vector3 GetReflectionPosition(Vector3 L, Vector3 surfaceNormal)
	{
		Vector3 newPos;
		newPos = L - 2 * (Vector3.Dot(L, surfaceNormal)) * surfaceNormal;
		//newPos *= range;
		newPos.z = 0;
		return newPos;
	}


	private Vector3 GetRefractionExitPosition(int i, Vector3 surfaceNormal, Vector3 L, float C)
	{
		float R = (1.1f + (i * 0.05f)) / 1f;// RefractionIndexExternal / RefractionIndexInternal;
											//from https://en.wikipedia.org/wiki/Snell%27s_law

		//TODO: switch the sign on this?
		float x = 1 - (R * R * (1 - (C * C)));
		if (x < 0)
		{
			x = -x;
			//Debug.Log(i + " is less than zero");
		}
		Vector3 newPos = (R * L) +
				(((R * C) - Mathf.Sqrt(x)) * surfaceNormal);
		newPos.Normalize();
		//newPos *= range;
		newPos.z = 0f;
		return newPos;
	}

	protected void OnLaserActivated()
	{
		if (LaserActivated != null)
		{
			LaserActivated();
		}
		metaball.SetActive(true);
		FMODUnity.RuntimeManager.PlayOneShot(LaserOn);
	}

	protected void OnLaserDeactivated()
	{
		//hitTarget = false;
		if (LaserDeactivated != null)
		{

			LaserDeactivated();
		}
		FMODUnity.RuntimeManager.PlayOneShot(LaserOff);

		StopAllCoroutines();

		root.rayLR.positionCount = 2;
		root.rayLR.SetPosition(0, transform.position);
		root.rayLR.SetPosition(1, transform.position);
		root.hitObject = null;
		metaball.SetActive(false);
		//Debug.Log("root child count " + root.children.Count);
		if (root.children.Count > 0)
		{
			root.PruneWholeTree();
			//Debug.Log("root child count " + root.children.Count);
		}

	}


	protected void OnTargetHit(GameObject target, RayNode rn)
	{
		//if (!hitTarget)
		//{
		//target.GetComponent<Target>().OnHitByTargetColor(rn);
		//	hitTarget = true;
		//}
		StartCoroutine(TargetHit(target, rn));
	}

	IEnumerator TargetHit(GameObject target, RayNode rn)
	{
		while (true)
		{
			target.GetComponent<Target>().OnHitByTargetColor(rn);
			yield return null;
		}
	}
}