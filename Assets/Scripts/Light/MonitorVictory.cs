﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonitorVictory : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		FindObjectOfType<TargetAwareness>().VictoryAchieved += OnVictory;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	protected void OnVictory()
	{
		//gameObject.SetActive(false);
		Destroy(gameObject);
	}
}
