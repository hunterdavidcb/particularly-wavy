﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargeObject : MonoBehaviour 
{
	public delegate void ChargeHandler(GameObject go);
	public event ChargeHandler Charged;
	public Image image;
	public Material targetMat;
	public GameObject Connection;
	public GameObject metaball;
	public bool LockOnceFullyCharged;

	public float chargeTime;
	float currentChargeTime;
	bool hit;
	bool fullyCharged;
	bool first = true;
	// Use this for initialization
	private void OnEnable()
	{
		Connection.GetComponent<Connection>().parent = this.gameObject;
	}

	void Start () 
	{
		currentChargeTime = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ((!hit && !LockOnceFullyCharged && !fullyCharged) || (!hit && LockOnceFullyCharged && !fullyCharged) 
			||(!hit && fullyCharged && !LockOnceFullyCharged))
		{
			currentChargeTime = Mathf.Clamp(currentChargeTime - Time.deltaTime, 0f, 5f);
			image.fillAmount = currentChargeTime / chargeTime;
			fullyCharged = false;
			//StartCoroutine(Reset());
		}
		else if (!fullyCharged && hit)
		{
			currentChargeTime = Mathf.Clamp(currentChargeTime + Time.deltaTime, 0f, 5f);
			image.fillAmount = currentChargeTime / chargeTime;
		}

		hit = false;
		metaball.SetActive(false);

		if (image.fillAmount == 1f)
		{
			fullyCharged = true;
			if (LockOnceFullyCharged)
			{
				if (first)
				{
					first = false;
					OnCharged();
				}
			}
			else
			{
				OnCharged();
			}
			
			
		}
	}

	public void OnHitByTargetMaterial(Material mat)
	{
		if (mat.color == targetMat.color)
		{
			hit = true;
			metaball.SetActive(true);
		}
	}

	protected void OnCharged()
	{
		Debug.Log("charged");
		if (Charged != null)
		{
			Charged(gameObject);
			metaball.SetActive(true);
		}
	}

}
