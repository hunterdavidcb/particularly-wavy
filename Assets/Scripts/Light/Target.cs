﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Target : MonoBehaviour 
{
	public delegate void TargetHandler(GameObject go, int maxDepth);
	public event TargetHandler TargetReached;
	public Material[] targetMaterials;
	bool reached = false;
	public bool isExclusive;
	public Image image;
	Dictionary<RayNode, bool> hitByMaterial;
	int counter;
	int maxDepth;

	private void Awake()
	{
		GrabMove[] gm = FindObjectsOfType<GrabMove>();
		for (int i = 0; i < gm.Length; i++)
		{
			gm[i].ObjectMoved += OnObjectMoved;
		}

		if (true)
		{

		}
	}

	void Start () 
	{
		hitByMaterial = new Dictionary<RayNode, bool>();
		if (isExclusive)
		{
			image.enabled = true;
		}
		else
		{
			image.enabled = false;
		}

		Color c = new Color();
		if (targetMaterials != null)
		{
			for (int i = 0; i < targetMaterials.Length; i++)
			{
				c += targetMaterials[i].color;
			}
		}

		Laser[] ls = FindObjectsOfType<Laser>();
		for (int i = 0; i < ls.Length; i++)
		{
			ls[i].LaserDeactivated += OnLaserDeactivated;
		}
		
		GetComponent<MeshRenderer>().material.color = c;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (counter == targetMaterials.Length && hitByMaterial.Count == targetMaterials.Length && isExclusive)
		{
			image.enabled = false;
			OnTargetReached(maxDepth);
		}
		else if (!reached )
		{
			image.enabled = true;
		}
	}

	private void OnObjectMoved()
	{
		hitByMaterial.Clear();
		image.enabled = true;
	}

	//use this to send a message from the laser to the target object
	public void OnHitByTargetColor(RayNode rn)
	{
		if (hitByMaterial.ContainsKey(rn))
		{
			hitByMaterial[rn] = true;
		}
		else
		{
			hitByMaterial.Add(rn, true);
		}

		counter = 0;
		maxDepth = -1;
		if (!isExclusive)
		{
			
			foreach (var item in hitByMaterial)
			{
				for (int i = 0; i < targetMaterials.Length; i++)
				{
					if (item.Value && item.Key.rayLR.material.color == targetMaterials[i].color)
					{
						counter++;
						if (item.Key.depth > maxDepth)
						{
							maxDepth = item.Key.depth;
						}
					}
				}
			}
			if (counter == targetMaterials.Length)
			{
				image.enabled = false;
				OnTargetReached(maxDepth);
			}
		}
		else
		{
			foreach (var item in hitByMaterial)
			{
				for (int i = 0; i < targetMaterials.Length; i++)
				{
					if (item.Value && item.Key.rayLR.material.color == targetMaterials[i].color)
					{
						counter++;
						if (item.Key.depth > maxDepth)
						{
							maxDepth = item.Key.depth;
						}
					}
				}
			}
			//Debug.Log("counter "  + counter);
			//Debug.Log("hitbymaterial count " + hitByMaterial.Count);	
		}
	}

	protected void OnLaserDeactivated()
	{
		hitByMaterial.Clear();
	}

	protected void OnTargetReached(int maxDepth)
	{
		reached = true;
		if (TargetReached != null)
		{
			TargetReached(gameObject, maxDepth);
		}

		if (transform.parent.GetComponent<Connection>() != null)
		{
			transform.parent.GetComponent<Connection>().Journey(transform.parent.GetComponent<Connection>().connectedObject);
		}
	}
	// on laser deactivated must send a message to the target so that it can be updated

	public void UpdateLasers(Laser laser)
	{
		laser.LaserDeactivated += OnLaserDeactivated;
	}
}
