﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour 
{
	public Transform pieceHolder;
	public Transform mainChunk;
	public GameObject part;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Spawn(Transform t)
	{
		Instantiate(part, t.position,Quaternion.identity);
	}

	public void DestroyStuff(Transform t)
	{
		if (t == mainChunk)
		{
			Debug.Log("Destroying main piece");
			Destroy(mainChunk.gameObject);
			for (int i = 0; i < pieceHolder.childCount; i++)
			{
				pieceHolder.GetChild(i).gameObject.SetActive(true);
			}
		}
		else
		{
			Destroy(t.gameObject);
		}
		
		
	}

}
