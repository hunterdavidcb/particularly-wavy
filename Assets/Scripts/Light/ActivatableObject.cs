﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatableObject : MonoBehaviour 
{
	public enum ActivationType
	{
		Expand,
		Contract,
		Rotate,
		ActivateLaser
	}

	float rotSpeed = 10f;

	public ActivationType at;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Activate()
	{
		switch (at)
		{
			case ActivationType.Expand:
				break;
			case ActivationType.Contract:
				break;
			case ActivationType.Rotate:
				Debug.Log("Rotate");
				StartCoroutine(Rotate());
				break;
			case ActivationType.ActivateLaser:
				Debug.Log("activating laser");
				for (int i = 0; i < transform.childCount; i++)
				{
					transform.GetChild(i).gameObject.SetActive(true);
				}
				
				break;
			default:
				break;
		}
	}

	IEnumerator Rotate()
	{
		Vector3 rot = transform.rotation.eulerAngles;
		Vector3 dRot = new Vector3(0f, 0f, -90f);
		while (rot.z > -90f)
		{
			//Debug.Log("Rotating");
			rot.z -= Time.deltaTime*rotSpeed;
			Debug.Log(rot.z);
			transform.rotation = Quaternion.Euler(rot);
			yield return null;
		}
	}
}
