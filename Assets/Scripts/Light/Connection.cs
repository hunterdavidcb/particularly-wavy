﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connection : MonoBehaviour
{
	public GameObject connectedObject;
	public GameObject particleObject;
	public GameObject parent;
	bool started = false;
	//private Vector3 screenPoint;
	//private Vector3 offset;
	//static int finalPosition = 20;
	//float angle = 10;
	//float radianAngle;
	//LineRenderer line;
	//Vector3[] points = new Vector3[finalPosition + 1];


	private void OnEnable()
	{
		//Debug.Log(transform.parent);
		//
	}
	// Use this for initialization
	void Start()
	{
		//line = GetComponent<LineRenderer>();
		//line.positionCount = finalPosition + 1;
		//for (int i = 0; i < line.positionCount; i++)
		//{
		//	line.SetPosition(i, transform.position);
		//	points[i] = transform.position;
		//}
		//line.widthCurve = new AnimationCurve(new Keyframe(0, 0.15625f), new Keyframe(1, 0.15625f));
		if (transform.parent != null)
		{
			if (parent.GetComponent<ChargeObject>() != null)
			{
				parent.GetComponent<ChargeObject>().Charged += Journey;
			}
		}
		
		
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void Journey(GameObject go)
	{
		if (!started)
		{
			//particleObject.GetComponent<ParticleSystem>().Play();
			StartCoroutine(Go());
			started = true;
		}
		
	}

	IEnumerator Go()
	{
		float timer = 0;
		float dist = Vector3.Distance(connectedObject.transform.position, particleObject.transform.position);
		float f;
		GameObject go = Instantiate(particleObject, transform.position, Quaternion.identity);
		go.GetComponent<ParticleSystem>().Play();
		go.transform.GetChild(0).gameObject.SetActive(true);
		while (Vector3.Distance(connectedObject.transform.position, go.transform.position) > 0.1f)
		{
			timer += Time.deltaTime;
			f = timer / dist;
			go.transform.position = Vector3.Lerp(transform.position, connectedObject.transform.position, f);
			yield return null;
		}
		Debug.Log("Arrived");
		go.GetComponent<ParticleSystem>().Stop();
		connectedObject.GetComponent<ActivatableObject>().Activate();
		Destroy(go);

	}

	//public void OnMouseDown()
	//{
	//	screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

	//	offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	//}

	//public void OnMouseDrag()
	//{

	//	Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

	//	Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
	//	curPosition.y = Mathf.Clamp(curPosition.y, 0, float.MaxValue);
		
	//	line.SetPosition(finalPosition, curPosition);
	//	points[finalPosition] = curPosition;
	//	//SetArc(curPosition);
	//	//points = LineSmoother.SmoothLine(points, 0.15f);
	//	//line.positionCount = points.Length;
	//	//line.SetPositions(points);

	//}

	//public void OnMouseUp()
	//{
	//	Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

	//	Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
	//	Collider[]  col = Physics.OverlapSphere(curPosition, 0.25f);
	//	foreach (var item in col)
	//	{
	//		if (item.gameObject.tag == "Connector" && item.gameObject != gameObject)
	//		{
	//			if (connectedObject == null)
	//			{
	//				Debug.Log("newly connected");
	//				line.SetPosition(finalPosition, curPosition);
	//				connectedObject = item.gameObject;
	//				item.GetComponent<Connection>().SetConnection(gameObject);
	//			}
	//			else if (connectedObject != item.gameObject && item.GetComponent<Connection>().connectedObject != gameObject)
	//			{
	//				Debug.Log("Resetting connection");
	//				connectedObject.GetComponent<Connection>().SetConnection(null);
	//				connectedObject = item.gameObject;
	//				line.SetPosition(finalPosition, curPosition);
	//				item.GetComponent<Connection>().SetConnection(gameObject);
	//			}
	//			else if (item.GetComponent<Connection>().connectedObject == gameObject)
	//			{
	//				line.SetPosition(finalPosition, line.GetPosition(0));
	//				Debug.Log("previously connected");
	//			}
	//			else
	//			{
	//				line.SetPosition(finalPosition, curPosition);
	//				Debug.Log("unknown result");
	//			}
				
	//			break;
	//		}
	//		else
	//		{
	//			line.SetPosition(finalPosition, line.GetPosition(0));
	//			//connectedObject = null;
	//			break;
	//		}
	//	}

	//	if (col.Length == 0)
	//	{
	//		connectedObject = null;
	//		line.SetPosition(finalPosition, line.GetPosition(0));
	//	}	
	//}

	//public void SetConnection(GameObject go)
	//{
	//	if (go != gameObject)
	//	{
	//		if (connectedObject != null)
	//		{
	//			for (int i = 0; i < line.positionCount; i++)
	//			{
	//				connectedObject.GetComponent<LineRenderer>().SetPosition(i, connectedObject.transform.position);
	//			}
	//			//connectedObject.GetComponent<LineRenderer>().SetPositions(new Vector3[2] { connectedObject.transform.position,
	//			//	connectedObject.transform.position });
	//			connectedObject.GetComponent<Connection>().connectedObject = null;
	//			line.SetPosition(finalPosition, transform.position);
	//			if (go != null)
	//			{
	//				Debug.Log("Now connected to " + go.name);
	//			}
	//			Debug.Log("Now disconnected");
	//			connectedObject = go;
	//		}
	//		else
	//		{
	//			Debug.Log("Newly connected to " + go.name);
	//			connectedObject = go;
	//		}
			
	//	}
		
	//}

	//void SetArc(Vector3 cp)
	//{

	//	line.SetPositions(CalculateArcArray(cp));
	//}

	//Vector3[] CalculateArcArray(Vector3 cp)
	//{
	//	radianAngle = Mathf.Deg2Rad * angle;
	//	float maxDistance = cp.x;// -transform.position.x;//Vector3.Distance(transform.position, cp);// Mathf.Clamp(Vector3.Distance(transform.position, cp), 0, 5f);//5f;

	//	for (int i = 1; i < points.Length; i++)
	//	{
	//		float t = (float)i / (float)points.Length;
	//		points[i] = CalculatePoint(t,maxDistance);
	//	}
	//	return points;
	//}

	//Vector3 CalculatePoint(float t,float maxDistance)
	//{
	//	float x = transform.position.x + t * maxDistance;
	//	float y = transform.position.y + x * Mathf.Tan(radianAngle) - ((x*x)/(2f*Mathf.Cos(radianAngle)* Mathf.Cos(radianAngle)));
	//	return new Vector3(x, -y, 0f);
	//}
}
