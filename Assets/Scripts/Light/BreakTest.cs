﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakTest : MonoBehaviour 
{
	Vector3 laserOrigin;
	Vector3 laserDirection;
	Transform parent;
	RaycastHit hit;
	float range = 20f;
	RayNode root;

	float timerMax = 0.25f;
	float timer;
	Transform hitObject;

	
	// Use this for initialization
	void Start () 
	{
		timer = timerMax;
		laserOrigin = transform.position;
		parent = transform.parent;
		laserDirection = (transform.position - parent.position).normalized;
		root = GetComponent<RayNode>();
		root.rayLR = GetComponent<LineRenderer>();
		root.rayLR.positionCount = 2;
		root.rayLR.SetPosition(0, laserOrigin);
		root.rayLR.SetPosition(1, laserOrigin + laserDirection*range);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Physics.SphereCast(laserOrigin,.1f,laserDirection,out hit, range))
		{
			Vector3 pos = hit.point + hit.normal * .1f + laserDirection*.3f;
			pos.z = 0f;
			root.rayLR.SetPosition(1, pos);

			Debug.Log("Hit " + hit.collider.gameObject.name);
			if (hitObject != hit.collider.transform)
			{
				hitObject = hit.collider.transform;
				timer = timerMax;
				hitObject.GetComponentInParent<Breakable>().Spawn(hitObject.transform);
			}
			else
			{
				timer -= Time.deltaTime;
				//hitObject.GetComponent<MeshRenderer>().material.color = Color.Lerp()
			}

			if (timer < 0)
			{
				//needs to check if the object is breakable
				hitObject.GetComponentInParent<Breakable>().DestroyStuff(hitObject);
				//Destroy(hitObject.gameObject);
			}
		}
		else
		{
			root.rayLR.SetPosition(1, laserOrigin + laserDirection*range);
		}
	}
}
