﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour 
{
	public float maxX;
	public float maxY;
	public float minX;
	public float minY;
	// Use this for initialization
	void Start () 
	{
		maxX = GetComponent<BoxCollider>().bounds.max.x;
		maxY = GetComponent<BoxCollider>().bounds.max.y;
		minX = GetComponent<BoxCollider>().bounds.min.x;
		minY = GetComponent<BoxCollider>().bounds.min.y;
		//Debug.Log(maxX);
		//Debug.Log(maxY);
		//Debug.Log(minX);
		//Debug.Log(minY);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
