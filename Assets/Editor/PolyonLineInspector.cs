﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(LinePolygonRenderer))]
public class PolygonLineInspector : Editor 
{
	private void OnSceneGUI()
	{
		LinePolygonRenderer line = target as LinePolygonRenderer;
		if (line.points.Count == 0)
		{
			line.points.Add(new Vector3(1,0,0));
			line.points.Add(new Vector3(1, 1, 0));
		}
		Transform handleTransform = line.transform;
		Quaternion handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;

		for (int i = 0; i < line.points.Count; i++)
		{
			Vector3 p0 = handleTransform.TransformPoint(line.points[i]);
			EditorGUI.BeginChangeCheck();
			Handles.DoPositionHandle(p0, handleRotation);
			p0 = Handles.PositionHandle(p0, handleRotation);
			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(line, "Move Point");
				EditorUtility.SetDirty(line);
				line.points[i] = handleTransform.InverseTransformPoint(p0);
			}

			if (i < line.points.Count && i-1 >=0)
			{
				Handles.DrawLine(line.points[i-1], line.points[i]);
			}
		}
		
		//Vector3 p0 = handleTransform.TransformPoint(line.points[0]);

		
		//Vector3 p1 = handleTransform.TransformPoint(line.points[1]);

		//Handles.color = Color.white;
		
		//EditorGUI.BeginChangeCheck();
		//Handles.DoPositionHandle(p0, handleRotation);
		//p0 = Handles.PositionHandle(p0, handleRotation);
		//if (EditorGUI.EndChangeCheck())
		//{
		//	Undo.RecordObject(line, "Move Point");
		//	EditorUtility.SetDirty(line);
		//	line.points[0] = handleTransform.InverseTransformPoint(p0);
		//}

		//EditorGUI.BeginChangeCheck();
		//Handles.DoPositionHandle(p1, handleRotation);
		//p1 = Handles.PositionHandle(p1, handleRotation);
		//if (EditorGUI.EndChangeCheck())
		//{
		//	Undo.RecordObject(line, "Move Point");
		//	EditorUtility.SetDirty(line);
		//	line.points[1] = handleTransform.InverseTransformPoint(p1);
		//}

		//Handles.DrawLine(p0, p1);
	}


	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("points"), true);
		serializedObject.ApplyModifiedProperties();
	}


}
