﻿using UnityEditor;
using UnityEngine;


namespace DatabaseTut
{
	public class DatabaseEditor : EditorWindow
	{
		private const float WINDOW_MIN_HEIGHT = 600f;
		private const float WINDOW_MIN_WIDTH = 800f;
		GUISkin DataEditorSkin;
		

		private enum Tabs
		{
			Actor, Item, Weapon, Armor
		}

		private Tabs currentTab;
		private bool _editMode;
		private int _selectedIndex;
		private IIdentity _data;
		private Vector2 _scrollPosition;
		private Vector2 _editScrollPos;

		[MenuItem("Window/Data Editor")]
		public static void GetWindow()
		{
			DatabaseEditor window = GetWindow<DatabaseEditor>("Data Editor", true);
			window.minSize = new Vector2(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT);
		}

		public void OnEnable()
		{
			currentTab = Tabs.Actor;
			Reset();
			Game.Load();
			DataEditorSkin = (GUISkin)Resources.Load("DataEditor");
		}

		private void OnGUI()
		{
			ShowTabs();
			ShowContent();
		}

		private void ShowTabs()
		{
			if (_editMode)
			{
				GUILayout.Toolbar((int)currentTab, System.Enum.GetNames(typeof(Tabs)));
			}
			else
			{
				currentTab = (Tabs)GUILayout.Toolbar((int)currentTab, System.Enum.GetNames(typeof(Tabs)));
			}
			
		}

		private void ShowContent()
		{
			EditorGUILayout.BeginHorizontal(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
			switch (currentTab)
			{
				case Tabs.Actor:
					//ShowSelection<Database<ActorData>, ActorData>(Game.actors);
					//ShowEditView<Database<ActorData>, ActorData>(Game.actors);
					break;
				case Tabs.Item:
					ShowSelection<Database<LevelData>, LevelData>(Game.levels);
					ShowEditView<Database<LevelData>, LevelData>(Game.levels);
					break;
				case Tabs.Weapon:
					//ShowSelection<Database<WeaponData>, WeaponData>(Game.weapons);
					//ShowEditView<Database<WeaponData>, WeaponData>(Game.weapons);
					break;
				case Tabs.Armor:
					//ShowSelection<Database<ArmorData>, ArmorData>(Game.armors);
					//ShowEditView<Database<ArmorData>, ArmorData>(Game.armors);
					break;
				default:
					break;
			}
			EditorGUILayout.EndHorizontal();
		}

		private void ShowSelection<T, U>(T database) where T:Database<U> where U:IIdentity, new()
		{
			EditorGUILayout.BeginVertical("Box",GUILayout.ExpandHeight(true),GUILayout.Width(WINDOW_MIN_WIDTH/3f));
			GUILayout.Label("Selection");
			_scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
			for (int i = 0; i < database.Length; i++)
			{
				EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true), GUILayout.Height(20f));
				GUILayout.Label(string.Format("[{0}] {1}",database.GetAt(i).ID, database.GetAt(i).Name));
				if (!_editMode)
				{
					if (GUILayout.Button("Edit", GUILayout.Width(40f)))
					{
						_editMode = true;
						_selectedIndex = i;
						_data = database.GetAt(i);

					}
					if (GUILayout.Button("Delete", GUILayout.Width(50f)))
					{
						database.RemoveAt(i);
						SaveChanges();
						break;
					}
				}
				
				
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndScrollView();
			if (GUILayout.Button("Add New", _editMode ? GUI.skin.button : DataEditorSkin.GetStyle("DisabledButton"), GUILayout.ExpandWidth(true)))
			{
				if (!_editMode)
				{
					_editMode = true;
					_selectedIndex = -1;
					_data = new U();
				}

			}
			EditorGUILayout.EndVertical();
			
		}

		private void SaveChanges()
		{
			Game.Save();
			Reset();
		}

		private void Reset()
		{
			_scrollPosition = Vector2.zero;
			_editScrollPos = Vector2.zero;
			_editMode = false;
			_selectedIndex = -1;
			GUI.FocusControl(null);
		}

		private void ShowEditView<T, U>(T database) where T : Database<U> where U : IIdentity, new()
		{
			EditorGUILayout.BeginVertical("Box", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
			if (_editMode)
			{
				_editScrollPos = EditorGUILayout.BeginScrollView(_editScrollPos, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
				_data.ShowEditFields();
				EditorGUILayout.EndScrollView();
				EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
				if (GUILayout.Button("Save"))
				{
					if (_selectedIndex == -1)
					{
						database.Add((U)_data);
					}
					else
					{
						database.Replace(_selectedIndex, (U)_data);
					}
					SaveChanges();
				}
				if (GUILayout.Button("Cancel"))
				{
					Reset();
				}
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndVertical();
		}
	}
}
